package com.orengy.archinator

import com.google.common.base.CaseFormat
import com.squareup.kotlinpoet.*
import io.requery.Entity
import io.requery.Persistable
import java.io.File
import javax.annotation.processing.AbstractProcessor
import javax.annotation.processing.ProcessingEnvironment
import javax.annotation.processing.RoundEnvironment
import javax.lang.model.SourceVersion
import javax.lang.model.element.Element
import javax.lang.model.element.ElementKind
import javax.lang.model.element.TypeElement
import javax.lang.model.type.ExecutableType
import javax.tools.Diagnostic

class Generator : AbstractProcessor() {

    @Synchronized
    override fun init(processingEnv: ProcessingEnvironment) {
        super.init(processingEnv)
        outDir = processingEnv.options[KAPT_KOTLIN_GENERATED_OPTION_NAME]?.replace("kaptKotlin", "kapt")!!
    }

    override fun getSupportedAnnotationTypes(): MutableSet<String> {
        println("getSupportedAnnotationTypes ")
        return mutableSetOf(
                Entity::class.java.name
        )
    }

    override fun getSupportedSourceVersion(): SourceVersion {
        return SourceVersion.latest()
    }

    override fun process(set: MutableSet<out TypeElement>, roundEnv: RoundEnvironment): Boolean {

        val recordClasses = mutableListOf<String>()
        var pack = ""


        val records = roundEnv.getElementsAnnotatedWith(Entity::class.java)
        for (it in records) {
            System.err.println("Processing: " + it.simpleName.toString())
            println("Processing: " + it.simpleName.toString())
            generateDeserializer(it)
            recordClasses.add(it.simpleName.toString())
            pack = processingEnv.elementUtils.getPackageOf(it).toString()
        }

        val unmarkedNodes = mutableListOf<String>()
        val nodeMark = hashMapOf<String, NodeMark>()
        val edges = hashMapOf<String, MutableList<String>>()
        val orderedRecords = mutableListOf<String>()
        for (it in records) {
            nodeMark[it.simpleName.toString()] = NodeMark.UNMARKED
            unmarkedNodes.add(it.simpleName.toString())
            edges[it.simpleName.toString()] = mutableListOf()
        }
        for (r in records) {
            r.enclosedElements.filter {
                it.kind == ElementKind.METHOD &&
                        it.simpleName.startsWith("get") &&
                        nodeMark.containsKey((it.asType() as ExecutableType).returnType.asTypeName().toString().substringAfterLast("."))

            }.forEach {
                val t = (it.asType() as ExecutableType).returnType.asTypeName().toString().substringAfterLast(".")
                if (!edges[r.simpleName.toString()]!!.contains(t))
                    edges[r.simpleName.toString()]!!.add(t)
            }
        }

//        edges.entries.forEach {
//            processingEnv.messager.printMessage(Diagnostic.Kind.NOTE, it.key + " -> " + it.value.joinToString())
//        }

        while (!unmarkedNodes.isEmpty()) {
            visit(unmarkedNodes.first(), unmarkedNodes, nodeMark, edges, orderedRecords)
        }

//        processingEnv.messager.printMessage(Diagnostic.Kind.NOTE, orderedRecords.joinToString(separator = " -> "))


        if (records.isEmpty()) {
            return true
        }

        generateSyncUtils(orderedRecords, pack)
        generateServerStateItemDeserializer(orderedRecords, pack)

        return true
    }

    internal fun visit(
            node: String,
            unmarkedNodes: MutableList<String>,
            nodeMark: HashMap<String, NodeMark>,
            edges: HashMap<String, MutableList<String>>,
            orderedNodes: MutableList<String>) {
        if (nodeMark[node] == NodeMark.MARKED)
            return
        if (nodeMark[node] == NodeMark.TEMPORARY_MARKED) {
            throw Exception("Cycle in records")
        }

        nodeMark[node] = NodeMark.TEMPORARY_MARKED
        for (m in edges[node]!!) {
            visit(m, unmarkedNodes, nodeMark, edges, orderedNodes)
        }
        nodeMark[node] = NodeMark.MARKED
        unmarkedNodes.remove(node)
        orderedNodes.add(node)
    }

    internal enum class NodeMark {
        UNMARKED,
        TEMPORARY_MARKED,
        MARKED
    }

    private fun generateDeserializer(element: Element) {

        val className = element.simpleName.toString()
        val pack = processingEnv.elementUtils.getPackageOf(element).toString()

        val deserializer = TypeSpec.classBuilder(className + "Deserializer")
                .addSuperinterface(
                        ParameterizedTypeName.get(
                                ClassName("com.google.gson", "JsonDeserializer"),
                                ClassName(pack, className)
                        )
                )
                .addFunction(FunSpec.builder("deserialize")
                        .addModifiers(KModifier.OVERRIDE)
                        .addParameter(
                                "json",
                                ClassName("com.google.gson", "JsonElement").asNullable())
                        .addParameter(
                                "typeOfT",
                                ClassName("java.lang.reflect", "Type").asNullable())
                        .addParameter(
                                "context",
                                ClassName("com.google.gson", "JsonDeserializationContext").asNullable())
                        .returns(ClassName(pack, className).asNullable())
                        .addCode(
                                "return context?.deserialize<${className}Entity>(json, ${className}Entity::class.java)"
                        )
                        .build())

        val file = FileSpec.builder(pack, className)
                .addType(deserializer.build())
                .addStaticImport("com.google.gson.reflect", "TypeToken")
                .addStaticImport(pack, "SyncUtils.Types")
                .build()


        file.writeTo(File(outDir, "${className}Deserializer.kt"))
    }

    private fun generateSyncUtils(recordClasses: List<String>, pack: String) {

        val typesConstants = TypeSpec.objectBuilder("Types")
        for (r in recordClasses) {
            typesConstants.addProperty(PropertySpec.builder(
                    CaseFormat.UPPER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, r), String::class)
                    .mutable(false)
                    .initializer("\"" + CaseFormat.UPPER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, r) + "\"")
                    .build())
        }


        val util = TypeSpec.objectBuilder("SyncUtils")
                .addType(
                        typesConstants.build()
                )
                .addFunction(FunSpec.builder("getEntityByType")
                        .addParameter(
                                "data",
                                ParameterizedTypeName.get(
                                        ClassName("io.requery.kotlin", "BlockingEntityStore"),
                                        Persistable::class.asClassName())
                        )
                        .addParameter(
                                "type",
                                ClassName("kotlin", "String"))
                        .addParameter(
                                "id",
                                ClassName("kotlin", "Int"))
                        .returns(ClassName("com.orengy.archinator.model.orm.base", "Record").asNullable())
                        .addCode("" +
                                "return when (type) {\n" +
                                recordClasses.joinToString(separator = "\n") {
                                    "Types.${CaseFormat.UPPER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, it)} -> data.findByKey($it::class, id)"
                                } +
                                "\n   else -> null\n" +
                                "}"
                        )
                        .build())
                .addProperty(PropertySpec.builder(
                        "records", ParameterizedTypeName.get(
                        ClassName("kotlin.collections", "List"),
                        ClassName("com.orengy.archinator.model.orm.base", "Record")))
                        .mutable(false)
                        .initializer("" +
                                "listOf<Record>(" +
                                recordClasses.joinToString(separator = ", ") {
                                    "${it}Entity()"
                                } +
                                ")"
                        )
                        .build())
                .addProperty(PropertySpec.builder(
                        "recordGson",
                        ClassName("com.google.gson", "Gson"))
                        .mutable(false)
                        .initializer("" +
                                "GsonBuilder()\n" +
                                ".setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)" +
                                recordClasses.joinToString(separator = "\n") {
                                    ".registerTypeAdapter($it::class.java, ${it}Deserializer())"
                                } +
                                ".create()"
                        )
                        .build())

        val file = FileSpec.builder(pack, "SyncUtils")
                .addType(util.build())
                .addStaticImport("com.google.gson", "GsonBuilder")
                .addStaticImport("com.google.gson", "FieldNamingPolicy")
                .build()

        file.writeTo(File(outDir, "SyncUtils.kt"))
    }

    private fun generateServerStateItemDeserializer(recordClasses: List<String>, pack: String) {

        val deserializer = TypeSpec.classBuilder("SyncServerStateItemModelDeserializer")
                .addSuperinterface(
                        ParameterizedTypeName.get(
                                ClassName("com.google.gson", "JsonDeserializer"),
                                ParameterizedTypeName.get(
                                        ClassName("com.orengy.archinator.model.response", "SyncServerStateItemModel"),
                                        ClassName("com.orengy.archinator.model.orm.base", "Record")
                                )
                        )
                )
                .addFunction(FunSpec.builder("deserialize")
                        .addModifiers(KModifier.OVERRIDE)
                        .addParameter(
                                "json",
                                ClassName("com.google.gson", "JsonElement").asNullable())
                        .addParameter(
                                "typeOfT",
                                ClassName("java.lang.reflect", "Type").asNullable())
                        .addParameter(
                                "context",
                                ClassName("com.google.gson", "JsonDeserializationContext").asNullable())
                        .returns(ParameterizedTypeName.get(
                                ClassName("com.orengy.archinator.model.response", "SyncServerStateItemModel"),
                                ClassName("com.orengy.archinator.model.orm.base", "Record")
                        ).asNullable())
                        .addCode("" +
                                "val type = json?.asJsonObject?.get(\"type\")?.asString\n" +
                                "return when (type) {\n" +
                                recordClasses.joinToString(separator = "\n") {
                                    "Types.${CaseFormat.UPPER_CAMEL.to(CaseFormat.UPPER_UNDERSCORE, it)} -> SyncUtils.recordGson.fromJson(json, object : TypeToken<SyncServerStateItemModel<${it}Entity>>() {}.type)"
                                } +
                                "\n    else -> null\n" +
                                "}"
                        )
                        .build())

        val file = FileSpec.builder(pack, "SyncServerStateItemModelDeserializer")
                .addType(deserializer.build())
                .addStaticImport("com.google.gson.reflect", "TypeToken")
                .addStaticImport(pack, "SyncUtils.Types")
                .build()


        file.writeTo(File(outDir, "SyncServerStateItemModelDeserializer.kt"))
    }

    lateinit var outDir: String


    companion object {
        const val KAPT_KOTLIN_GENERATED_OPTION_NAME = "kapt.kotlin.generated"
    }
}