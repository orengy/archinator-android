package com.orengy.archinator

import android.accounts.Account
import android.accounts.AccountManager
import android.content.Context
import android.net.ConnectivityManager
import android.provider.Settings
import android.support.multidex.MultiDexApplication
import android.util.Log
import com.google.gson.GsonBuilder
import com.orengy.archinator.model.orm.Models
import com.orengy.archinator.utils.Constants
//import com.google.gson.GsonBuilder
import com.orengy.archinator.utils.Params
import com.orengy.archinator.utils.Utils
import io.requery.Persistable
import io.requery.android.sqlite.DatabaseSource
import io.requery.reactivex.KotlinReactiveEntityStore
import io.requery.sql.KotlinEntityDataStore
import io.requery.sql.TableCreationMode
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit

//import okhttp3.Cache
//import okhttp3.Interceptor
//import okhttp3.OkHttpClient
//import okhttp3.logging.HttpLoggingInterceptor
//import retrofit2.Retrofit
//import retrofit2.converter.gson.GsonConverterFactory

class ArchinatorApplication : MultiDexApplication() {

    val data: KotlinReactiveEntityStore<Persistable> by lazy {

        val source = DatabaseSource(this, Models.DEFAULT, "archinator.db", 1)
        source.setTableCreationMode(TableCreationMode.DROP_CREATE)
        source.setLoggingEnabled(true)
        val store = KotlinEntityDataStore<Persistable>(source.configuration)

        KotlinReactiveEntityStore<Persistable>(store)
    }

    override fun onCreate() {
        super.onCreate()

        appContext = applicationContext


        Params.LOCAL_ENV_BASE_URL.value = "http://10.248.1.225"
        Params.ENVIRONMENT.value = Utils.Environment.LOCAL.name


        val pInfo = packageManager.getPackageInfo(packageName, 0)
        appVersion = "android-" + pInfo.versionName + "-" + pInfo.versionCode
        deviceCode = "android_" + Settings.Secure.getString(applicationContext.contentResolver, Settings.Secure.ANDROID_ID)

        setUpRetrofit()


//        FacebookSdk.sdkInitialize(getApplicationContext())
//        AppEventsLogger.activateApp(this)
    }

    companion object {
        lateinit var deviceCode: String
        lateinit var appVersion: String

        lateinit var appContext: Context
        lateinit var restAdapter: Retrofit

    }

    var user: Account? = null
        get() = field
        set(value) {
            field = value
            val manager = getSystemService(Context.ACCOUNT_SERVICE) as AccountManager
            userEmail = manager.getUserData(value, Constants.Auth.ARG_USER_EXTRA_DATA_EMAIL)
            userName = manager.getUserData(value, Constants.Auth.ARG_USER_EXTRA_DATA_NAME)

        }

    var userEmail: String? = null
    var userName: String? = null

    private fun setUpRetrofit() {
        var httpCacheDir: File? = null
        var cache: Cache? = null
        try {
            httpCacheDir = File(applicationContext.cacheDir, "httpResponses")
            val httpCacheSize = (10 * 1024 * 1024).toLong() // 10 MiB
            cache = Cache(httpCacheDir, httpCacheSize)

        } catch (e: Exception) {
            Log.e("Retrofit", "Could not create http cache", e)
        }

        val httpClient = OkHttpClient.Builder()
        httpClient.interceptors().add(interceptor)
        httpClient.connectTimeout(10, TimeUnit.SECONDS)
        httpClient.readTimeout(10, TimeUnit.SECONDS)
        httpClient.writeTimeout(10, TimeUnit.SECONDS)
//        httpClient.cache(cache)

        val okHttpClient = httpClient.build()



        restAdapter = Retrofit.Builder()
                .baseUrl(Utils.baseUrl)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create(Utils.gson))
                .build()
    }

//    fun isOnline(): Boolean {
//        val cm = applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
//        val netInfo = cm.activeNetworkInfo
//        return netInfo != null && netInfo.isConnectedOrConnecting
//    }

    internal var interceptor: Interceptor = Interceptor { chain ->
        var request = chain.request()

//        val accessToken = UserHelper.getAccessToken()
//
//        val timestamp = SecurityHelper.GetEstimatedServerTime()

//        var url = request.url().toString()
//        if (url.indexOf('?') > 0) {
//            url = url.substring(0, url.indexOf('?'))
//        }

        request = request.newBuilder()
//                .addHeader("Timestamp", timestamp.toString())
//                .addHeader("Access-Key", SecurityHelper.GenerateAPIKey(timestamp, url))
                .addHeader("Device-Code", deviceCode)
                .addHeader("App-Version", appVersion)
//                .addHeader("Access-Token", UserUtils.accessToken)
                .addHeader("Api-Mode", "true")
//                .addHeader("Content-Language", "zh-CN")
                .build()
        chain.proceed(request)
    }
}