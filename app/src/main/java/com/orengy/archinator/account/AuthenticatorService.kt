package com.orengy.archinator.account

import android.app.Service
import android.content.Intent
import android.os.IBinder
import java.net.Authenticator

/**
 * Created by Adomas on 2017-10-26.
 */
class AuthenticatorService : Service() {
    // Instance field that stores the authenticator object
    private var mAuthenticator: ArchinatorAuthenticator? = null

    override fun onCreate() {
        // Create a new authenticator object
        mAuthenticator = ArchinatorAuthenticator(this)
    }

    /*
     * When the system binds to this Service to make the RPC call
     * return the authenticator's IBinder.
     */
    override fun onBind(intent: Intent): IBinder? {
        return mAuthenticator!!.getIBinder()
    }

}