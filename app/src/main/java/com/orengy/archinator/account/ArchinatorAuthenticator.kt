package com.orengy.archinator.account

import android.accounts.AbstractAccountAuthenticator
import android.accounts.Account
import android.accounts.AccountAuthenticatorResponse
import android.content.Context
import android.os.Bundle
import android.accounts.AccountManager
import android.R.attr.accountType
import android.content.Intent
import com.orengy.archinator.utils.Constants
import android.accounts.AccountManager.KEY_BOOLEAN_RESULT
import android.R.attr.name
import android.text.TextUtils
import android.content.ContentValues.TAG
import com.orengy.archinator.activity.AuthenticatorActivity


/**
 * Created by Adomas on 2017-10-26.
 */
class ArchinatorAuthenticator(val context: Context) : AbstractAccountAuthenticator(context) {
    override fun getAuthTokenLabel(authTokenType: String?): String? {
        return authTokenType
    }

    override fun confirmCredentials(
            response: AccountAuthenticatorResponse?,
            account: Account?,
            options: Bundle?
    ): Bundle? {
        return null
    }

    override fun updateCredentials(
            response: AccountAuthenticatorResponse?,
            account: Account?,
            authTokenType: String?,
            options: Bundle?
    ): Bundle? {
        return null
    }

    override fun getAuthToken(
            response: AccountAuthenticatorResponse?,
            account: Account?,
            authTokenType: String?,
            options: Bundle?
    ): Bundle? {
        val am = AccountManager.get(context)

        val authToken = am.peekAuthToken(account, authTokenType)

        if (!TextUtils.isEmpty(authToken)) {
            val result = Bundle()
            result.putString(AccountManager.KEY_ACCOUNT_NAME, account?.name)
            result.putString(AccountManager.KEY_ACCOUNT_TYPE, account?.type)
            result.putString(AccountManager.KEY_AUTHTOKEN, authToken)
            return result
        }

        val intent = Intent(context, AuthenticatorActivity::class.java)
        intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response)
        intent.putExtra(Constants.Auth.ARG_ACCOUNT_TYPE, account?.type)
        intent.putExtra(Constants.Auth.ARG_AUTH_TYPE, authTokenType)
        val bundle = Bundle()
        bundle.putParcelable(AccountManager.KEY_INTENT, intent)
        return bundle
    }

    override fun hasFeatures(
            response: AccountAuthenticatorResponse?,
            account: Account?,
            features: Array<out String>?
    ): Bundle? {
        val result = Bundle()
        result.putBoolean(KEY_BOOLEAN_RESULT, false)
        return result
    }

    override fun editProperties(
            response: AccountAuthenticatorResponse?,
            accountType: String?
    ): Bundle? {
        return null
    }

    override fun addAccount(
            response: AccountAuthenticatorResponse?,
            accountType: String?,
            authTokenType: String?,
            requiredFeatures: Array<out String>?,
            options: Bundle?
    ): Bundle {
        val intent = Intent(context, AuthenticatorActivity::class.java)
        intent.putExtra(Constants.Auth.ARG_ACCOUNT_TYPE, accountType)
        intent.putExtra(Constants.Auth.ARG_AUTH_TYPE, authTokenType ?: Constants.Auth.AUTH_TOKEN_TYPE_FULL_ACCESS)
        intent.putExtra(Constants.Auth.ARG_IS_ADDING_NEW_ACCOUNT, true)
        intent.putExtra(AccountManager.KEY_ACCOUNT_AUTHENTICATOR_RESPONSE, response)

        val bundle = Bundle()
        bundle.putParcelable(AccountManager.KEY_INTENT, intent)
        return bundle
    }

}