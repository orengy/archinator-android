package com.orengy.archinator.sync

import android.app.Service
import android.content.Intent
import android.os.IBinder


/**
 * Created by Adomas on 2017-10-28.
 */
class SyncService : Service() {
    override fun onCreate() {
        synchronized(sSyncAdapterLock) {
            if (sSyncAdapter == null) {
                sSyncAdapter = SyncAdapter(applicationContext, true)
            }
        }
    }

    override fun onBind(intent: Intent): IBinder {
        return sSyncAdapter!!.syncAdapterBinder
    }

    companion object {
        private var sSyncAdapter: SyncAdapter? = null
        private val sSyncAdapterLock = Any()
    }
}