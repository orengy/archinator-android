package com.orengy.archinator.sync

import android.accounts.Account
import android.accounts.AccountManager
import android.content.*
import android.net.Uri
import android.os.Bundle
import android.util.Log
import com.orengy.archinator.ArchinatorApplication
import com.orengy.archinator.activity.MainActivity
import com.orengy.archinator.model.orm.*
import com.orengy.archinator.model.orm.base.Record
import com.orengy.archinator.model.orm.base.SyncDownRecord
import com.orengy.archinator.model.orm.base.SyncFileRecord
import com.orengy.archinator.model.orm.base.SyncRecord
import com.orengy.archinator.model.sync.SyncBaseModel
import com.orengy.archinator.model.sync.SyncDownStatusModel
import com.orengy.archinator.model.request.SyncRequest
import com.orengy.archinator.model.sync.SyncFullModel
import com.orengy.archinator.model.sync.SyncStatusModel
import com.orengy.archinator.utils.Constants
import com.orengy.archinator.utils.Utils
import org.jetbrains.anko.startActivity


/**
 * Created by Adomas on 2017-10-28.
 */
class SyncAdapter : AbstractThreadedSyncAdapter {
    constructor(context: Context, autoInitialize: Boolean) : super(context, autoInitialize)
    constructor(context: Context,
                autoInitialize: Boolean,
                allowParallelSyncs: Boolean) : super(context, autoInitialize, allowParallelSyncs)

    override fun onPerformSync(
            account: Account?,
            extras: Bundle?,
            authority: String?,
            provider: ContentProviderClient?,
            syncResult: SyncResult
    ) {

        val manager = context.getSystemService(Context.ACCOUNT_SERVICE) as AccountManager

        val authToken = manager.blockingGetAuthToken(account, Constants.Auth.AUTH_TOKEN_TYPE_FULL_ACCESS, true)


//        val records = listOf<Record>(
//                DistanceEntity(),
//                TargetFaceEntity(),
//                TargetFaceSizeEntity(),
//                ColorEntity(),
//                EquipmentArrowEntity()
////                SessionEntity(),
////                RoundEntity(),
////                EndEntity(),
////                EndArrowEntity(),
////                EndMediaEntity()
//        )

        val records = SyncUtils.records

        val data = (context.applicationContext as ArchinatorApplication).data.toBlocking()

        val syncRequest = SyncRequest(context)

        for (r in records) {
            if (r is SyncFileRecord) {
                val result = data.select(r::class)
//                        .where(r::localFileUrl eq null )
                        .get().iterator()
                for (item in result) {
                    val i = item as SyncFileRecord
                    if (i.localFileUrl == null) {
                        continue
                    }
                    val resp = syncRequest.file(authToken, Uri.parse(i.localFileUrl))
                    i.localFileUrl = null
                    i.remoteFileUrl = resp.remoteFileUrl
                    data.update(i)
                }
            }

        }

        val syncItems = mutableListOf<SyncBaseModel>()

        for (r in records) {
            val result = data.select(r::class).get().iterator()
            if (r is SyncDownRecord) {
                for (item in result) {
                    syncItems.add(
                            SyncDownStatusModel<SyncDownRecord>(item as SyncDownRecord)
                    )
                }
            } else if (r is SyncRecord) {
                for (item in result) {
                    val item = item as SyncRecord
                    if (item.id > 0 && !item.dirty) {
                        syncItems.add(
                                SyncStatusModel<SyncRecord>(item)
                        )
                    } else {
                        syncItems.add(
                                SyncFullModel<SyncRecord>(item)
                        )
                    }
                }
            }
        }
        Log.d("sync", Utils.gson.toJson(syncItems))
        val res = syncRequest.sync(authToken, syncItems)

        Log.d("sync", Utils.gson.toJson(res))

        val recordSortOrder = hashMapOf<String, Int>()
        for ((i, r) in SyncUtils.records.withIndex()) {
            recordSortOrder[r.type] = i

        }

        val serverState = res.serverState?.filter { it != null }?.sortedBy {
            recordSortOrder[it?.type]!!
        } ?: return

        loop@ for (i in serverState) {
            syncResult.stats.numEntries++
            when (i!!.action) {
                Constants.Record.Action.DELETE -> {
                    val record: Record = SyncUtils.getEntityByType(data, i.type, i.id!!)
                            ?: continue@loop
                    data.delete(record)
                    syncResult.stats.numDeletes++
                }
                Constants.Record.Action.UPDATE -> {
                    if (i.data is SyncRecord) {
                        i.data.syncUpdate = true
                    }
                    data.update(i.data as Record)
                    syncResult.stats.numUpdates++
                }
                Constants.Record.Action.INSERT -> {
                    data.insert(i.data as Record)
                    syncResult.stats.numInserts++
                }
            }
        }


    }
}