package com.orengy.archinator.model.orm.base

import com.orengy.archinator.utils.Utils
import io.requery.Superclass

/**
 * Created by Adomas on 2017-10-28.
 */
@Superclass
abstract class SyncFileRecord(type: String) : SyncRecord(type) {
    abstract var localFileUrl: String?
    abstract var remoteFileUrl: String?

    fun getFileUrl(): String? {
        if (localFileUrl != null)
            return localFileUrl
        else
            return Utils.baseUrl + remoteFileUrl
    }
}