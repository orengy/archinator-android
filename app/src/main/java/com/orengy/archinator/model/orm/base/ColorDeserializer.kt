package com.orengy.archinator.model.orm.base

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.orengy.archinator.model.orm.Color
import com.orengy.archinator.model.orm.ColorEntity
import com.orengy.archinator.model.orm.TargetFace
import com.orengy.archinator.model.orm.TargetFaceEntity
import java.lang.reflect.Type

/**
 * Created by Adomas on 2017-11-05.
 */
class ColorDeserializer : JsonDeserializer<Color> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Color? {
        return context?.deserialize<Color>(json, ColorEntity::class.java)
    }
}