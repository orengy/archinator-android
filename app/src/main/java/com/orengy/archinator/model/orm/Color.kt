package com.orengy.archinator.model.orm

import com.orengy.archinator.model.orm.base.SyncDownRecord
import com.orengy.archinator.utils.Constants
import io.requery.Entity

/**
 * Created by Adomas on 2017-10-26.
 */
@Entity(stateless = true)
abstract class Color : SyncDownRecord(SyncUtils.Types.COLOR) {
    abstract var name: String
    abstract var hex: Int
    abstract var code: String
}