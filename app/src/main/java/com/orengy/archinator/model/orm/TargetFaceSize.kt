package com.orengy.archinator.model.orm

import com.orengy.archinator.model.orm.base.SyncDownRecord
import com.orengy.archinator.utils.Constants
import io.requery.Entity
import io.requery.ManyToOne

/**
 * Created by Adomas on 2017-10-26.
 */
@Entity(stateless = true)
abstract class TargetFaceSize : SyncDownRecord(SyncUtils.Types.TARGET_FACE_SIZE) {
    abstract var name: String

    @get:ManyToOne
    abstract var targetFace: TargetFace
}