package com.orengy.archinator.model.orm

import com.orengy.archinator.model.orm.base.SyncRecord
import com.orengy.archinator.utils.Constants
import io.requery.Entity

/**
 * Created by Adomas on 2017-10-26.
 */
@Entity(stateless = true)
abstract class Camera : SyncRecord(SyncUtils.Types.CAMERA) {
    abstract var deviceCode: String
    abstract var calibratedJson: String
    abstract var processing: Boolean
}