package com.orengy.archinator.model.sync

import com.orengy.archinator.model.orm.base.Record

/**
 * Created by Adomas on 2017-10-28.
 */
abstract class SyncBaseModel(val type: String) {
    abstract val id: Int
//    abstract val serverId: Int?
}