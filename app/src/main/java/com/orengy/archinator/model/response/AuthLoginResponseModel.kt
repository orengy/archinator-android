package com.orengy.archinator.model.response

/**
 * Created by Adomas on 2017-11-01.
 */
class AuthLoginResponseModel : BaseModel() {
    lateinit var accessToken: String
    lateinit var uid: String
    var name: String? = null
    var email: String? = null

}