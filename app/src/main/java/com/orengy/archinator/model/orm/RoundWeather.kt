package com.orengy.archinator.model.orm

import com.orengy.archinator.model.orm.base.SyncRecord
import com.orengy.archinator.utils.Constants
import io.requery.Entity
import io.requery.ManyToOne
import io.requery.OneToMany

/**
 * Created by Adomas on 2017-10-26.
 */
@Entity(stateless = true)
abstract class RoundWeather : SyncRecord(SyncUtils.Types.ROUND_WEATHER) {
    @get:ManyToOne
    abstract var round: Round

    abstract var time: Int
    abstract var temperature: Int
    abstract var humidity: Int
    abstract var windSpeed: Double
    abstract var windDirection: Int
}