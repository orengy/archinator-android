package com.orengy.archinator.model.sync

import com.orengy.archinator.model.orm.base.SyncRecord

/**
 * Created by Adomas on 2017-10-28.
 */
class SyncFullModel<T : SyncRecord>(val data: T) : SyncBaseModel(data.type) {
    override val id: Int = data.id
    val updatedAt = data.updatedAt
//    override val serverId: Int? = data.remoteId
}