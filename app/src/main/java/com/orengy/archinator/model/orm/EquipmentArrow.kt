package com.orengy.archinator.model.orm

import com.orengy.archinator.model.orm.base.SyncRecord
import com.orengy.archinator.utils.Constants
import io.requery.Entity
import io.requery.ManyToOne
import io.requery.OneToMany

/**
 * Created by Adomas on 2017-10-26.
 */
@Entity(stateless = true)
abstract class EquipmentArrow : SyncRecord(SyncUtils.Types.EQUIPMENT_ARROW) {

    abstract var diameterInMm: Double
    abstract var name: String

    @get:ManyToOne
    abstract var fletchingOneColor: Color
    @get:ManyToOne
    abstract var fletchingTwoColor: Color
    @get:ManyToOne
    abstract var fletchingThreeColor: Color
    @get:ManyToOne
    abstract var nockColor: Color
    @get:ManyToOne
    abstract var shaftColor: Color

}