package com.orengy.archinator.model.orm.base

import io.requery.Superclass

/**
 * Created by Adomas on 2017-10-27.
 */
@Superclass
abstract class SyncDownRecord(type: String) : Record(type) {
//    abstract var remoteId: Int?
}