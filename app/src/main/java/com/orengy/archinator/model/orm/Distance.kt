package com.orengy.archinator.model.orm

import com.orengy.archinator.model.orm.base.SyncDownRecord
import com.orengy.archinator.utils.Constants
import io.requery.*

/**
 * Created by Adomas on 2017-10-26.
 */
@Entity(stateless = true)
abstract class Distance : SyncDownRecord(SyncUtils.Types.DISTANCE) {
    abstract var distance: Int

    @get:ManyToOne
    abstract var unit: Unit
}