package com.orengy.archinator.model.sync

import com.orengy.archinator.model.orm.base.SyncDownRecord
import com.orengy.archinator.model.orm.base.SyncRecord

/**
 * Created by Adomas on 2017-10-28.
 */
class SyncDownStatusModel<T : SyncDownRecord>(data: T) : SyncBaseModel(data.type) {
    override val id: Int = data.id
//    override val serverId: Int? = data.remoteId
}