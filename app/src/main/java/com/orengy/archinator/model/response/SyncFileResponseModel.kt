package com.orengy.archinator.model.response

import com.orengy.archinator.model.orm.base.Record

/**
 * Created by Adomas on 2017-11-01.
 */
class SyncFileResponseModel : BaseModel() {
    lateinit var remoteFileUrl: String
}