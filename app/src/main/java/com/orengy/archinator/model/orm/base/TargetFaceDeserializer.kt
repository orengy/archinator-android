package com.orengy.archinator.model.orm.base

import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.orengy.archinator.model.orm.TargetFace
import com.orengy.archinator.model.orm.TargetFaceEntity
import java.lang.reflect.Type

/**
 * Created by Adomas on 2017-11-05.
 */
class TargetFaceDeserializer : JsonDeserializer<TargetFace> {
    override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): TargetFace? {
        return context?.deserialize<TargetFaceEntity>(json, TargetFaceEntity::class.java)
    }
}