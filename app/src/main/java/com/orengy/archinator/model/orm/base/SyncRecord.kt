package com.orengy.archinator.model.orm.base

import io.requery.*

/**
 * Created by Adomas on 2017-10-27.
 */

@Superclass
abstract class SyncRecord(type: String) : Record(type) {
    abstract var updatedAt: Long?

    abstract var createdAt: Long

    @get:Column(value = "0")
    abstract var dirty: Boolean

    var syncUpdate = false

    @PreUpdate
    fun beforeUpdate() {
        if (!syncUpdate) {
            dirty = true
            updatedAt = System.currentTimeMillis()
        }
    }

    @PreInsert
    fun beforeInsert() {
        createdAt = System.currentTimeMillis()
    }

    @PreDelete
    fun beforeDelete() {

    }

}