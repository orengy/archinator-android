package com.orengy.archinator.model.request


import android.content.Context
import com.orengy.archinator.ArchinatorApplication
import com.orengy.archinator.model.response.AuthLoginResponseModel
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

/**
 * Created by Adomas on 2017-06-27.
 */
class AuthRequest(ctx: Context) : BaseRequest(ctx) {
    val api: ApiInterface = ArchinatorApplication.restAdapter.create(ApiInterface::class.java)

    interface ApiInterface {
        @POST("/v1/auth/login")
        @FormUrlEncoded
        fun login(
                @Field("provider") provider: String,
                @Field("token") token: String,
                @Field("identity") identity: String
        ): Call<AuthLoginResponseModel>
    }

    fun login(provider: String, token: String, identity: String,
              listener: BaseResponse<AuthLoginResponseModel>): BaseRequest {
        completeCall(api.login(provider, token, identity), listener)
        return this
    }

}