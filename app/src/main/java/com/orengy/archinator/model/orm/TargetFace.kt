package com.orengy.archinator.model.orm

import com.orengy.archinator.model.orm.base.SyncDownRecord
import com.orengy.archinator.utils.Constants
import io.requery.Entity
import io.requery.OneToMany

/**
 * Created by Adomas on 2017-10-26.
 */
@Entity(stateless = true)
abstract class TargetFace : SyncDownRecord(SyncUtils.Types.TARGET_FACE) {
    abstract var name: String


//    abstract var diameter: Int
//    abstract var unit: String
}