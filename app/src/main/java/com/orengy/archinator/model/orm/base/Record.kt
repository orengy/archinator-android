package com.orengy.archinator.model.orm.base

import com.orengy.archinator.utils.Constants
import io.requery.*
import io.requery.kotlin.BlockingEntityStore
import com.orengy.archinator.ArchinatorApplication
import com.orengy.archinator.model.orm.*


/**
 * Created by Adomas on 2017-10-27.
 *
 * +Create new orm
 * ++Constants
 * ++Sync Adapter
 * ++Record class
 * ++recordGson
 * ++SyncServerStateItemModelDeserializer
 * ++Record deserializer
 */
@Superclass
abstract class Record(val type: String) : Persistable {

    //    @get:Generated
    @get:Key
    abstract var id: Int


    @PreInsert
    fun t1() {
        if (id == 0)
            id = assignId(this)
    }

    companion object {
        private val newEntityId = hashMapOf<String, Int>()

        private fun assignId(entity: Record): Int {
            if (!newEntityId.contains(entity.type)) {
                val data = (ArchinatorApplication.appContext as ArchinatorApplication).data.toBlocking()
                val res = data.select(entity::class).limit(1).get().firstOrNull()
                if (res != null) {
                    newEntityId[entity.type] = if (res.id > 0) -1 else (res.id - 1)
                } else {
                    newEntityId[entity.type] = -1
                }
            }
            val c = newEntityId[entity.type] ?: 0
            newEntityId[entity.type] = c - 1
            return c
        }

//        fun getEntityByType(data: BlockingEntityStore<Persistable>, type: String, id: Int): Record? {
//            return when (type) {
//                SyncUtils.Types.DISTANCE -> data.findByKey(Distance::class, id)
//                SyncUtils.Types.TARGET_FACE -> data.findByKey(TargetFace::class, id)
//                SyncUtils.Types.TARGET_FACE_SIZE -> data.findByKey(TargetFaceSize::class, id)
//                SyncUtils.Types.COLOR -> data.findByKey(Color::class, id)
//                SyncUtils.Types.EQUIPMENT_ARROW -> data.findByKey(EquipmentArrow::class, id)
//                else -> null
//            }
//        }
    }

}