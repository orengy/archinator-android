package com.orengy.archinator.model.request

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.support.v7.app.AlertDialog
import android.text.SpannableString
import android.text.method.LinkMovementMethod
import android.widget.TextView
import com.orengy.archinator.model.response.BaseModel
import com.orengy.archinator.utils.Utils

import org.json.JSONException
import org.json.JSONObject

import java.io.IOException
import java.net.SocketTimeoutException
import java.net.URLEncoder
import java.net.UnknownHostException

import javax.net.ssl.SSLHandshakeException

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Created by Adomas on 2016-12-30.
 */

abstract class BaseRequest(protected val context: Context) {
    private var loadingDialog: ProgressDialog? = null
    private var loadingText: String? = null
    private var requestInProcess = false
    private var showError = true


    class RequestException(message:String) : Exception(message)

    interface BaseResponse<T> {
        fun onSuccess(data: T)

        fun onFail(message: String)
    }

    fun <T> completeCall(call: Call<T>): T {
        val response = call.execute()
        val model = response.body()
        if (model != null) {
            return model
        } else {
            var error = "Code: " + response.code() + ". Data cannot be parsed."
            try {
                error += " Error Message: " + response.errorBody()!!.string()
            } catch (ignored: IOException) {
            }

           throw RequestException(error)
        }
    }

    fun <T> completeCall(call: Call<T>, listener: BaseResponse<T>) {
        requestInProcess = true
        showLoading()
        call.enqueue(object : Callback<T> {
            override fun onResponse(call: Call<T>, response: Response<T>) {
                requestInProcess = false
                dismissLoading()

                val model = response.body()
                if (model != null) {
                    listener.onSuccess(model)
                } else {
                    var error = "Code: " + response.code() + ". Data cannot be parsed."
                    try {
                        error += " Error Message: " + response.errorBody()!!.string()
                    } catch (ignored: IOException) {
                    }

                    handleRequestFailure(call, listener, "Data cannot be parsed.", error, true)
                }
            }

            override fun onFailure(call: Call<T>, t: Throwable) {
                requestInProcess = false
                dismissLoading()

                if (t is UnknownHostException
                        || t is SSLHandshakeException
                        || t is SocketTimeoutException) {
                    handleRequestNetworkFailure(call, listener)
                } else {
                    handleRequestFailure(call, listener, "Something went wrong", t.toString(), true)
                }
            }
        })
    }

    private fun <T> handleRequestFailure(
            call: Call<T>,
            listener: BaseResponse<T>,
            message: String,
            fullError: String?,
            canRetry: Boolean) {
        if (!showError) {
            listener.onFail(message)
            return
        }

        if (context is Activity) {
            val builder = AlertDialog.Builder(context)


            builder.setTitle("Error Loading")

            if (fullError != null) {
                val txt = TextView(context);
                val padding = Utils.convertToPx(20, context);
                txt.setPadding(padding, padding, padding, 0);

                val s: SpannableString = SpannableString(message);

                txt.setText(s);
                txt.setMovementMethod(LinkMovementMethod.getInstance());

                builder.setView(txt);
            } else {
                builder.setMessage(message);
            }


            if (canRetry) {
                builder
                        .setPositiveButton("Retry") { dialog, id -> completeCall(call.clone(), listener) }
                        .setNegativeButton("Cancel") { dialog, id -> }
            } else {
                builder.setPositiveButton("OK") { dialog, id -> listener.onFail(message) }
            }

            builder.setCancelable(false)

            val dialog = builder.create()
            dialog.show()

            return
        }

        listener.onFail(message)

    }

    private fun <T> handleRequestNetworkFailure(
            call: Call<T>,
            listener: BaseResponse<T>) {
        handleRequestFailure(call, listener, "Network Error", null, true)
    }


    fun hideErrorDialog(): BaseRequest {
        showError = false
        return this
    }

    fun showErrorDialog(): BaseRequest {
        showError = true
        return this
    }

    fun addLoadDialog(loadingText: String): BaseRequest {
        this.loadingText = loadingText
        showLoading()
        return this
    }

    fun addLoadDialog(resId: Int): BaseRequest {
        return addLoadDialog(context.getString(resId))
    }

    private fun showLoading() {
        if (requestInProcess && context is Activity && loadingText != null) {
            loadingDialog = ProgressDialog.show(context, "",
                    loadingText, true)
        }
    }

    private fun dismissLoading() {
        if (loadingDialog != null)
            loadingDialog!!.dismiss()
    }
}
