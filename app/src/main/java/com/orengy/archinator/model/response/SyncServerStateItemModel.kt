package com.orengy.archinator.model.response

/**
 * Created by Adomas on 2017-10-31.
 */
data class SyncServerStateItemModel<T>(
        val type: String,
        val action: String,
        val id: Int?,
//        val serverId: Int? = null,
        val data: T? = null
)