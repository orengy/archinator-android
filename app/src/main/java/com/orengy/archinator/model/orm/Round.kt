package com.orengy.archinator.model.orm

import com.orengy.archinator.model.orm.base.SyncFileRecord
import com.orengy.archinator.model.orm.base.SyncRecord
import com.orengy.archinator.utils.Constants
import io.requery.Entity
import io.requery.ManyToOne
import io.requery.OneToMany

/**
 * Created by Adomas on 2017-10-26.
 */
@Entity(stateless = true)
abstract class Round : SyncFileRecord(SyncUtils.Types.ROUND) {
    @get:ManyToOne
    abstract var session: Session

    abstract var arrowsPerEnd: Int
    abstract var endsCount: Int

    @get:ManyToOne
    abstract var targetFace: TargetFace
    @get:ManyToOne
    abstract var targetFaceSize: TargetFaceSize
    @get:ManyToOne
    abstract var targetFaceScores: TargetFaceScores

    @get:ManyToOne
    abstract var distance: Distance

}