package com.orengy.archinator.model.orm

import com.orengy.archinator.model.orm.base.SyncRecord
import com.orengy.archinator.utils.Constants
import io.requery.Entity
import io.requery.ManyToOne

/**
 * Created by Adomas on 2017-10-26.
 */
@Entity(stateless = true)
abstract class EndArrow : SyncRecord(SyncUtils.Types.END_ARROW) {
    @get:ManyToOne
    abstract var end: End
    abstract var posX: Double
    abstract var posY: Double
    abstract var score: Int
}