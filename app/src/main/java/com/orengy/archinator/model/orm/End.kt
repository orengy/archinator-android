package com.orengy.archinator.model.orm

import com.orengy.archinator.model.orm.base.SyncRecord
import com.orengy.archinator.utils.Constants
import io.requery.Entity
import io.requery.ManyToOne
import io.requery.OneToMany

/**
 * Created by Adomas on 2017-10-26.
 */
@Entity(stateless = true)
abstract class End : SyncRecord(SyncUtils.Types.END) {
    @get:ManyToOne
    abstract var round: Round
}