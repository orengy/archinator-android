package com.orengy.archinator.model.request

import android.content.Context
import android.net.Uri
import com.orengy.archinator.ArchinatorApplication
import com.orengy.archinator.model.response.SyncFileResponseModel
import com.orengy.archinator.model.response.SyncResponseModel
import com.orengy.archinator.model.response.SyncServerStateItemModel
import com.orengy.archinator.model.sync.SyncBaseModel
import com.orengy.archinator.utils.Utils
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*
import java.io.File

/**
 * Created by Adomas on 2017-10-31.
 */
class SyncRequest(ctx: Context) : BaseRequest(ctx) {
    val api: ApiInterface = ArchinatorApplication.restAdapter.create(ApiInterface::class.java)

    interface ApiInterface {
        @POST("/v1/sync")
        @FormUrlEncoded
        fun sync(
                @Header("Access-Token") accessToken: String,
                @Field("data") data: String
        ): Call<SyncResponseModel>

        @Multipart
        @POST("/v1/sync/file")
        fun file(
                @Header("Access-Token") accessToken: String,
                @Part data: MultipartBody.Part
        ): Call<SyncFileResponseModel>
    }

    fun sync(accessToken: String, items: List<SyncBaseModel>): SyncResponseModel {
        return completeCall(api.sync(accessToken, Utils.gson.toJson(items)))
    }

    fun file(accessToken: String, fileUri: Uri): SyncFileResponseModel {
        val file = File(fileUri.path)
//context.contentResolver.getType(fileUri)
        val b = RequestBody.create(MediaType.parse("image/jpeg"), file)
        val a = MultipartBody.Part.createFormData("data", file.name, b)

        return completeCall(api.file(accessToken, a))
    }
}