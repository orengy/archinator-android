package com.orengy.archinator.model.orm

import com.orengy.archinator.model.orm.base.SyncFileRecord
import com.orengy.archinator.model.orm.base.SyncRecord
import com.orengy.archinator.utils.Constants
import io.requery.Entity
import io.requery.ManyToOne

/**
 * Created by Adomas on 2017-10-26.
 */
@Entity(stateless = true)
abstract class EndMediaArrow : SyncRecord(SyncUtils.Types.END_MEDIA_ARROW) {
    @get:ManyToOne
    abstract var endMedia: EndMedia

    abstract var posX: Double

    abstract var posY: Double

    abstract var score: Int
}