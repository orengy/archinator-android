package com.orengy.archinator.model.orm

import com.orengy.archinator.model.orm.base.SyncFileRecord
import com.orengy.archinator.utils.Constants
import io.requery.Entity
import io.requery.ManyToOne

/**
 * Created by Adomas on 2017-10-26.
 */
@Entity(stateless = true)
abstract class EndMedia : SyncFileRecord(SyncUtils.Types.END_MEDIA) {
    @get:ManyToOne
    abstract var end: End

    abstract var processing: Boolean

    abstract var detectedArrows: Int

    abstract var processingTime: Long
}