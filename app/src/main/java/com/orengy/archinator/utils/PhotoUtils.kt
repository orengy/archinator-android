package com.orengy.archinator.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.content.FileProvider
import com.orengy.archinator.activity.BaseActivity
import com.orengy.archinator.fragment.BaseFragment
import com.orengy.archinator.fragment.CameraCalibrationFragment
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import android.graphics.Bitmap
import java.io.FileOutputStream
import android.content.DialogInterface
import android.R.array
import android.app.FragmentHostCallback
import android.support.v7.app.AlertDialog
import com.orengy.archinator.R
import org.opencv.core.Mat
import org.opencv.core.Size
import org.opencv.imgproc.Imgproc
import android.graphics.BitmapFactory
import com.orengy.archinator.common.MatHelper
import org.opencv.android.OpenCVLoader
import org.opencv.android.Utils


/**
 * Created by Adomas on 3/2/2018.
 */
object PhotoUtils {

    private var currentFile: File? = null
    private var lastRequestTakePicture = true
    private var lastRequestCode = 0

    private fun createImageFile(context: Context): File {
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
                imageFileName,
                ".jpg",
                storageDir
        )

        return image
    }

    private fun takePictureIntent(context: Context): Intent? {
        lastRequestTakePicture = true

        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)

        if (takePictureIntent.resolveActivity(context.packageManager) != null) {

            val photoFile: File =
                    try {
                        createImageFile(context)
                    } catch (ex: Throwable) {
                        return null
                    }
            currentFile = photoFile


            val photoURI = FileProvider.getUriForFile(context,
                    "com.orengy.archinator.fileprovider",
                    photoFile)
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)

            return takePictureIntent
        }
        return null
    }

    private fun choosePictureIntent(): Intent {
        lastRequestTakePicture = false
        return Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
    }

    fun handleResponse(context: Context, requestCode: Int, resultCode: Int, data: Intent?): String? {
        if (resultCode == Activity.RESULT_OK && requestCode == lastRequestCode && OpenCVLoader.initDebug()) {

            val bitmap = if (lastRequestTakePicture) {
                BitmapFactory.decodeFile(currentFile!!.absolutePath)
            } else {
                MediaStore.Images.Media.getBitmap(
                        context.contentResolver,
                        Uri.parse(data?.data.toString()))
                        ?: return null
            }


            val temp = Mat()
            Utils.bitmapToMat(bitmap, temp)
            MatHelper.resize(temp)
            val size = temp.size()
            val resized: Bitmap = Bitmap.createBitmap(size.width.toInt(), size.height.toInt(), Bitmap.Config.ARGB_8888)
            Utils.matToBitmap(temp, resized)


            val file = if (lastRequestTakePicture) {
                currentFile!!
            } else {
                createImageFile(context)
            }

            val fos = FileOutputStream(file)
            resized.compress(Bitmap.CompressFormat.JPEG, 100, fos)
            fos.close()

            currentFile = null

            return "file://${file.absolutePath}"


//            if (lastRequestTakePicture) {
//                val url = "file://${currentFile!!.absolutePath}"
//                val bmOptions = BitmapFactory.Options()
//                val bitmap = BitmapFactory.decodeFile(currentFile!!.absolutePath, bmOptions)
//
//                currentFile = null
//                return url
//            } else {
//                val bitmap = MediaStore.Images.Media.getBitmap(
//                        context.contentResolver,
//                        Uri.parse(data?.data.toString()))
//                        ?: return null
//                val resized = resize(bitmap)
//                val file = createImageFile(context)
//                val fos = FileOutputStream(file)
//                resized.compress(Bitmap.CompressFormat.JPEG, 100, fos)
//                fos.close()
//                return "file://${file.absolutePath}"
//            }
        }
        return null
    }

    private fun resize(src: Bitmap, width: Int = 768): Bitmap {
        val height = (src.height * width) / src.width
        return Bitmap.createScaledBitmap(src, width, height, true)
    }


    fun photoPicker(fragment: BaseFragment, requestCode: Int) {
        val builder = AlertDialog.Builder(fragment.context!!)
        val options = arrayOf("Gallery", "Take a photo")
        builder.setTitle("Choose method")
                .setItems(options, { dialog, which ->
                    lastRequestCode = requestCode
                    val intent = if (which == 0) {
                        choosePictureIntent()
                    } else {
                        takePictureIntent(fragment.context!!)
                    } ?: return@setItems

                    fragment.startActivityForResult(intent, requestCode)
                })
        builder.create().show()
    }

    fun photoPicker(activity: BaseActivity, requestCode: Int) {
        val builder = AlertDialog.Builder(activity)
        val options = arrayOf("Gallery", "Take a photo")
        builder.setTitle("Choose method")
                .setItems(options, { dialog, which ->
                    lastRequestCode = requestCode
                    val intent = if (which == 0) {
                        choosePictureIntent()
                    } else {
                        takePictureIntent(activity)
                    } ?: return@setItems

                    activity.startActivityForResult(intent, requestCode)
                })
        builder.create().show()
    }
}