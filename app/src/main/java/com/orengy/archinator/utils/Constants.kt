package com.orengy.archinator.utils

/**
 * Created by Adomas on 2017-10-28.
 */
object Constants {


    object Record {
//        object Type {
//            val EQUIPMENT_ARROW = "equipment_arrow"
//            val DISTANCE = "distance"
//            val COLOR = "color"
//            val END = "end"
//            val END_ARROW = "end_arrow"
//            val END_MEDIA = "end_media"
//            val END_MEDIA_ARROW = "end_media_arrow"
//            val ROUND = "round"
//            val SESSION = "session"
//            val TARGET_FACE = "target_face"
//            val TARGET_FACE_SIZE = "target_face_size"
//            val TARGET_FACE_SCORES = "target_face_scores"
//            val CAMERA_CALIBRATION = "camera_calibration"
//            val CAMERA = "camera"
//            val UNIT = "unit"
//        }

        object Action {
            val UPDATE = "update"
            val DELETE = "delete"
            val INSERT = "insert"
        }

    }

    object Sync {
        val AUTHORITY = "com.orengy.archinator"

        private val SECONDS_PER_MINUTE = 60L
        private val SYNC_INTERVAL_IN_MINUTES = 60L
        val SYNC_INTERVAL = SYNC_INTERVAL_IN_MINUTES * SECONDS_PER_MINUTE

    }

    object Auth {
        val AUTH_TOKEN_TYPE_FULL_ACCESS = "Full access"
        val ACCOUNT_TYPE = "com.orengy.archinator"

        val GOOGLE_APPS_SERVER_CLIENT_ID = "289860889293-35jvpd1g1n7akqf3s5igi7ntmkbtme90.apps.googleusercontent.com"

        val AUTHENTICATION_METHOD_GOOGLE = "google"

        val ARG_USER_EXTRA_DATA_NAME = "ARG_USER_EXTRA_DATA_NAME"
        val ARG_USER_EXTRA_DATA_UID = "ARG_USER_EXTRA_DATA_UID"
        val ARG_USER_EXTRA_DATA_EMAIL = "ARG_USER_EXTRA_DATA_EMAIL"

        val ARG_ACCOUNT_TYPE = "ACCOUNT_TYPE"
        val ARG_AUTH_TYPE = "AUTH_TYPE"
        val ARG_ACCOUNT_NAME = "ACCOUNT_NAME"
        val ARG_IS_ADDING_NEW_ACCOUNT = "IS_ADDING_ACCOUNT"
    }
}