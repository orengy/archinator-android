package com.orengy.archinator.utils


import android.content.Context
import com.orengy.archinator.ArchinatorApplication

/**
 * Created by Adomas on 2017-06-23.
 */

class Params<T> private constructor(
        private val key: String,
        private val prefKey: String = PREF_KEY,
        private val default: T? = null
) {

    companion object {
        val PREF_KEY = "archinator"
        val ENVIRONMENT = Params<String>("environment", default = Utils.Environment.PROD.name)
        val LOCAL_ENV_BASE_URL = Params<String>("local_env_base_url")
        val ACCESS_TOKEN = Params<String>("access_token")
        val UID = Params<String>("uid")

    }

    var value: T? = default
        get() {
            val sharedPref = ArchinatorApplication.appContext.getSharedPreferences(prefKey, Context.MODE_PRIVATE)
            return when (field) {
                is String? -> sharedPref.getString(key, default as String?) as T
                is Int? -> sharedPref.getInt(key, default as Int? ?: 0) as T
                is Long? -> sharedPref.getLong(key, default as Long? ?: 0) as T
                is Boolean? -> sharedPref.getBoolean(key, default as Boolean? ?: false) as T
                is Float? -> sharedPref.getFloat(key, default as Float? ?: 0f) as T
                else -> null
            }
        }
        set(value) {
            val sharedPref = ArchinatorApplication.appContext.getSharedPreferences(prefKey, Context.MODE_PRIVATE)
            val editor = sharedPref.edit()
            when (field) {
                is String? -> editor.putString(key, value as String?)
                is Int? -> editor.putInt(key, value as Int? ?: 0)
                is Long? -> editor.putLong(key, value as Long? ?: 0)
                is Boolean? -> editor.putBoolean(key, value as Boolean? ?: false)
                is Float? -> editor.putFloat(key, value as Float? ?: 0f)
            }

            editor.commit()
        }

}