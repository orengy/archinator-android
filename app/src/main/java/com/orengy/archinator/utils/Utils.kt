package com.orengy.archinator.utils

import android.content.Context
import android.text.Spannable
import android.text.util.Linkify
import android.util.TypedValue
import com.google.gson.FieldNamingPolicy
import com.google.gson.GsonBuilder
import com.orengy.archinator.BuildConfig
import com.orengy.archinator.model.orm.SyncServerStateItemModelDeserializer
import com.orengy.archinator.model.response.SyncServerStateItemModel
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

/**
 * Created by Adomas on 2017-06-27.
 */
object Utils {

    val gson = GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .registerTypeAdapter(SyncServerStateItemModel::class.java, SyncServerStateItemModelDeserializer())
            .create()

//    val recordGson = GsonBuilder()
//            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
//            .registerTypeAdapter(TargetFace::class.java, TargetFaceDeserializer())
//            .registerTypeAdapter(Color::class.java, ColorDeserializer())
//            .create()

    enum class Environment(val baseUrl: String) {
        LOCAL(""),
        DEV("http://dev.com"),
        PROD("http://prod.com")
    }

    val environment: Environment
        get() {
            when (Params.ENVIRONMENT.value) {
                Environment.DEV.name -> return Environment.DEV
                Environment.PROD.name -> return Environment.PROD
                else -> return Environment.LOCAL
            }
        }

    val baseUrl: String
        get() {
            val env = environment
            if (env == Environment.LOCAL) {
                return Params.LOCAL_ENV_BASE_URL.value ?: Environment.PROD.baseUrl
            }
            return env.baseUrl
        }

    val appIsInDebugMode: Boolean
        get() = BuildConfig.DEBUG


    fun formatTime(time: Long, formatter: SimpleDateFormat = SimpleDateFormat("MMM d, yyyy")): String {
        return formatter.format(Date(time))
    }


    /**
     * Add a link to the TextView which is given.

     * @param spannable      the field containing the text
     * *
     * @param patternToMatch a regex pattern to put a link around
     * *
     * @param link           the link to add
     */
    fun addLink(spannable: Spannable, patternToMatch: String,
                link: String) {
        val filter = Linkify.TransformFilter { match, url -> link }
        Linkify.addLinks(spannable, Pattern.compile(patternToMatch), "archinator", null,
                filter)
    }

    fun convertToPx(dp: Int, context: Context): Int {
        val r = context.resources
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp.toFloat(), r.displayMetrics).toInt()
    }

}