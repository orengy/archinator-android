package com.orengy.archinator.view

import android.content.Context
import android.support.v7.widget.LinearLayoutManager

/**
 * Created by Adomas on 2/19/2018.
 */
class ScrollableLinearLayoutManager(context: Context) : LinearLayoutManager(context) {

    var scrollEnabled: Boolean = true

    override fun canScrollVertically(): Boolean {
        return scrollEnabled && super.canScrollVertically()
    }

    override fun canScrollHorizontally(): Boolean {
        return scrollEnabled && super.canScrollHorizontally()
    }

}