package com.orengy.archinator.service

import android.app.IntentService
import android.content.ContentValues.TAG
import android.content.Intent
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import com.orengy.archinator.ArchinatorApplication
import com.orengy.archinator.activity.OpenCVActivity
import com.orengy.archinator.common.Arrows
import com.orengy.archinator.common.Camera
import com.orengy.archinator.common.MatHelper
import com.orengy.archinator.model.orm.End
import com.orengy.archinator.model.orm.EndMedia
import io.requery.Persistable
import io.requery.kotlin.BlockingEntityStore
import org.jetbrains.anko.toast
import org.opencv.android.BaseLoaderCallback
import org.opencv.android.LoaderCallbackInterface
import org.opencv.android.OpenCVLoader
import org.opencv.core.CvType
import org.opencv.core.Mat
import android.graphics.Bitmap
import android.net.Uri
import com.orengy.archinator.model.orm.EndArrowEntity
import com.orengy.archinator.model.orm.EndMediaArrowEntity
import io.requery.kotlin.eq
import org.opencv.android.Utils
import org.opencv.imgproc.Imgproc


/**
 * Created by Adomas on 2/10/2018.
 */
class ArrowProcessingService : IntentService("ArrowProcessingService") {
    companion object {
        const val ARG_END_MEDIA_ID = "end_media_id"
        const val ACTION_RESP = "com.orengy.archinator.intent.action.ARROW_PROCESSED"
    }

    override fun onHandleIntent(intent: Intent?) {
//        android.os.Debug.waitForDebugger();

        if (intent == null) {
            return
        }
        if (OpenCVLoader.initDebug()) {
            val endMediaId = intent.extras.getInt(ARG_END_MEDIA_ID, 0)

            val endMedia = data.findByKey(EndMedia::class, endMediaId) ?: return
            data.refresh(endMedia.end)
            if (endMedia.processing) {
                val startTime = System.currentTimeMillis()

                val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, Uri.parse(endMedia.localFileUrl))
                        ?: return
                val file = Mat()
                val temp = Mat()
                Utils.bitmapToMat(bitmap, temp)
                Imgproc.cvtColor(temp, file, Imgproc.COLOR_RGBA2BGR)

                val camera = Camera(9, 6)
                val cameraOrm = (data select com.orengy.archinator.model.orm.Camera::class where (com.orengy.archinator.model.orm.Camera::deviceCode eq ArchinatorApplication.deviceCode)).get().firstOrNull()
                        ?: return
                if (cameraOrm.calibratedJson.isEmpty() || cameraOrm.processing) {
                    return
                }
                camera.fromJsonString(cameraOrm.calibratedJson)

                MatHelper.resize(file)

                val img = Mat()
                camera.undistort(file, img)

                val arrows = Arrows(file, "")
                val scoredArrows = arrows.findArrows()


                for (arrow in scoredArrows) {
                    val p = arrows.target.pointToRealMetrics(arrow.tipPoint)

                    val endMediaArrowEntity = EndMediaArrowEntity()
                    endMediaArrowEntity.posX = p.x
                    endMediaArrowEntity.posY = p.y
                    endMediaArrowEntity.score = arrow.score
                    endMediaArrowEntity.endMedia = endMedia
                    data.insert(endMediaArrowEntity)

                    val endArrowEntity = EndArrowEntity()
                    endArrowEntity.posX = p.x
                    endArrowEntity.posY = p.y
                    endArrowEntity.score = arrow.score
                    endArrowEntity.end = endMedia.end
                    data.insert(endArrowEntity)
                }

                endMedia.processing = false
                endMedia.detectedArrows = scoredArrows.size
                endMedia.processingTime = System.currentTimeMillis() - startTime
                data.update(endMedia)


                val broadcastIntent = Intent()
                broadcastIntent.action = ACTION_RESP
                broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT)
                sendBroadcast(broadcastIntent)
            }
        }


    }

    lateinit var data: BlockingEntityStore<Persistable>

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        data = (application as ArchinatorApplication).data.toBlocking()
        return super.onStartCommand(intent, flags, startId)
    }
}