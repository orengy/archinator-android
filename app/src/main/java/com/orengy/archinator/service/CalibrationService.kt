package com.orengy.archinator.service

import android.app.IntentService
import android.content.ContentValues.TAG
import android.content.Intent
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import com.orengy.archinator.ArchinatorApplication
import com.orengy.archinator.activity.OpenCVActivity
import com.orengy.archinator.common.Arrows
import com.orengy.archinator.common.Camera
import com.orengy.archinator.common.MatHelper
import io.requery.Persistable
import io.requery.kotlin.BlockingEntityStore
import org.jetbrains.anko.toast
import org.opencv.android.BaseLoaderCallback
import org.opencv.android.LoaderCallbackInterface
import org.opencv.android.OpenCVLoader
import org.opencv.core.CvType
import org.opencv.core.Mat
import android.graphics.Bitmap
import android.net.Uri
import com.bumptech.glide.Glide
import com.orengy.archinator.model.orm.*
import io.requery.kotlin.eq
import org.opencv.android.Utils
import org.opencv.imgproc.Imgproc


/**
 * Created by Adomas on 2/10/2018.
 */
class CalibrationService : IntentService("CalibrationService") {
    companion object {
        const val ACTION_RESP = "com.orengy.archinator.intent.action.CALIBRATED"
    }

    override fun onHandleIntent(intent: Intent?) {
        if (intent == null) {
            return
        }
        if (OpenCVLoader.initDebug()) {
            val phoneCamera = Camera(9, 6)

            val camera = (data select com.orengy.archinator.model.orm.Camera::class where (com.orengy.archinator.model.orm.Camera::deviceCode eq ArchinatorApplication.deviceCode)).get().firstOrNull()

            if (camera.processing) {
                val images = (data select CameraCalibration::class where (CameraCalibration::camera eq camera)).get()

                val mats = mutableListOf<Mat>()

                for (img in images) {
                    val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, Uri.parse(img.localFileUrl))
                            ?: return
                    val temp = Mat()
                    Utils.bitmapToMat(bitmap, temp)
                    MatHelper.resize(temp)
                    Imgproc.cvtColor(temp, temp, Imgproc.COLOR_RGBA2GRAY)
                    mats.add(temp)
                }

                phoneCamera.calibrate(mats)

                camera.processing = false
                camera.calibratedJson = phoneCamera.toJsonString()
                data.update(camera)

                val broadcastIntent = Intent()
                broadcastIntent.action = ACTION_RESP
                broadcastIntent.addCategory(Intent.CATEGORY_DEFAULT)
                sendBroadcast(broadcastIntent)
            }
        }


    }

    lateinit var data: BlockingEntityStore<Persistable>

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        data = (application as ArchinatorApplication).data.toBlocking()
        return super.onStartCommand(intent, flags, startId)
    }
}