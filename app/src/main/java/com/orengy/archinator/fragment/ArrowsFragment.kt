package com.orengy.archinator.fragment


import android.app.Activity.RESULT_OK
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v7.app.ActionBar
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.TextView

import com.orengy.archinator.R
import com.orengy.archinator.activity.ArrowFormActivity
import com.orengy.archinator.activity.SessionFormActivity
import com.orengy.archinator.model.orm.End
import com.orengy.archinator.model.orm.EquipmentArrow
import com.orengy.archinator.model.orm.Models
import io.requery.android.QueryRecyclerAdapter
import io.requery.query.Result
import kotlinx.android.synthetic.main.fragment_arrows.*
import org.jetbrains.anko.imageResource
import org.jetbrains.anko.support.v4.startActivity
import org.jetbrains.anko.support.v4.startActivityForResult
import org.jetbrains.anko.support.v4.toast
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class ArrowsFragment : BaseFragment() {

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == RESULT_OK) {
            adapter.queryAsync()
        }
    }

    private lateinit var executor: ExecutorService
    private lateinit var adapter: ArrowAdapter
    private lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_arrows, container, false)

        executor = Executors.newSingleThreadExecutor()
        adapter = ArrowAdapter()
        adapter.setExecutor(executor)

        recyclerView = view.findViewById(R.id.arrowRecyclerView)

        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(activity)

        adapter.queryAsync()

        return view
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        fab?.startAnimation(AnimationUtils.loadAnimation(context, R.anim.grow))


    }

    override fun configureFab(fab: FloatingActionButton) {
        fab.imageResource = R.drawable.ic_add_white

        fab.setOnClickListener { v ->
            startActivityForResult<ArrowFormActivity>(ARROW_FORM_REQUEST)
        }
    }

    override fun configureActionBar(actionBar: ActionBar) {
        actionBar.title = "Arrows"
    }

    companion object {
        val ARROW_FORM_REQUEST = 1234

        fun newInstance(): ArrowsFragment {
            val fragment = ArrowsFragment()
            return fragment
        }
    }

    internal inner class ArrowHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        //        var image : ImageView? = null
        var name: TextView? = null
    }

    private inner class ArrowAdapter internal constructor() :
            QueryRecyclerAdapter<EquipmentArrow, ArrowHolder>(Models.DEFAULT, EquipmentArrow::class.java),
            View.OnClickListener, View.OnLongClickListener {

        override fun onLongClick(v: View?): Boolean {
            val equipmentArrow = v!!.tag as EquipmentArrow
            val builder = AlertDialog.Builder(this@ArrowsFragment.context!!)

            builder.setTitle("Do you want to delete this item?")

            builder.setPositiveButton("Yes") { dialog, which ->
                data.delete(equipmentArrow)
                this.queryAsync()
            }
            builder.setNegativeButton("Cancel") { dialog, which ->
            }

            val dialog = builder.create()
            dialog.show()

            return true
        }

        override fun performQuery(): Result<EquipmentArrow> {
            return (data select (EquipmentArrow::class)).get()
        }

        override fun onBindViewHolder(item: EquipmentArrow, holder: ArrowHolder, position: Int) {
            holder.name!!.text = item.name
//            holder.image!!.setBackgroundColor(colors[random.nextInt(colors.size)])
            holder.itemView.tag = item
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArrowHolder {
            val inflater = LayoutInflater.from(parent.context)
            val view = inflater.inflate(R.layout.item_arrow, parent, false)
            val holder = ArrowHolder(view)
            holder.name = view.findViewById<TextView>(R.id.arrowName)
//            holder.image = view.findViewById(R.id.picture) as ImageView
            view.setOnClickListener(this)
            view.setOnLongClickListener(this)
            return holder
        }

        override fun onClick(v: View) {
            val arrow = v.tag as EquipmentArrow? ?: return
//            toast(arrow.id.toString())

            startActivityForResult<ArrowFormActivity>(ARROW_FORM_REQUEST,
                    ArrowFormActivity.ARG_ARROW_ID to arrow.id)

//            startActivity<ArrowFormActivity>(
//                    ArrowFormActivity.ARG_ARROW_ID to arrow.id
//            )

//                val intent = Intent(this@PeopleActivity, PersonEditActivity::class.java)
//                intent.putExtra(PersonEditActivity.Companion.EXTRA_PERSON_ID, person.id)
//                startActivity(intent)
        }
    }
}
