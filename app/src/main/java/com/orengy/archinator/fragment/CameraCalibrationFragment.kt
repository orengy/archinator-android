package com.orengy.archinator.fragment


import android.app.Activity
import android.content.*
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v4.content.FileProvider
import android.support.v7.app.ActionBar
import android.support.v7.app.AlertDialog
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import com.bumptech.glide.Glide
import com.orengy.archinator.ArchinatorApplication

import com.orengy.archinator.R
import com.orengy.archinator.model.orm.*
import com.orengy.archinator.service.ArrowProcessingService
import com.orengy.archinator.service.CalibrationService
import com.orengy.archinator.utils.PhotoUtils
import io.requery.android.QueryRecyclerAdapter
import io.requery.kotlin.eq
import io.requery.query.Result
import kotlinx.android.synthetic.main.activity_end_form.*
import org.jetbrains.anko.imageResource
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.support.v4.startService
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.Executors

class CameraCalibrationFragment : BaseFragment() {

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val photoUrl = PhotoUtils.handleResponse(this.context!!, requestCode, resultCode, data)

        if (photoUrl != null) {
            val cameraCalibrationEntity = CameraCalibrationEntity()
            cameraCalibrationEntity.camera = camera
            cameraCalibrationEntity.localFileUrl = photoUrl
            this.data.insert(cameraCalibrationEntity)
            adapter.queryAsync()
        }
    }


    private lateinit var adapter: CameraCalibrationAdapter
    private lateinit var recyclerView: RecyclerView

    private lateinit var camera: Camera

    private lateinit var calibrationStatus: TextView
    private lateinit var calibrationButton: Button
    private lateinit var calibrationProgress: ProgressBar

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_camera_calibration, container, false)

        adapter = CameraCalibrationAdapter()
        adapter.setExecutor(Executors.newSingleThreadExecutor())

        recyclerView = view.findViewById(R.id.cameraCalibrationRecyclerView)

        calibrationStatus = view.findViewById(R.id.calibrationStatusTextView)
        calibrationButton = view.findViewById(R.id.calibrationButton)
        calibrationProgress = view.findViewById(R.id.calibrationProgressBar)

        calibrationButton.onClick {
            startService<CalibrationService>()
            camera.processing = true
            camera.calibratedJson = ""
            data.update(camera)
            updateCalibrationStatusView()
        }

        recyclerView.adapter = adapter
        recyclerView.layoutManager = GridLayoutManager(activity, 2)

        val cam = (data select Camera::class where (Camera::deviceCode eq ArchinatorApplication.deviceCode)).get().firstOrNull()
        if (cam == null) {
            val camEntity = CameraEntity()
            camEntity.deviceCode = ArchinatorApplication.deviceCode
            camEntity.calibratedJson = ""
            camEntity.processing = false
            data.insert(camEntity)
            camera = camEntity


        } else {
            camera = cam
        }

        updateCalibrationStatusView()

        adapter.queryAsync()

        val filter = IntentFilter(CalibrationService.ACTION_RESP)
        filter.addCategory(Intent.CATEGORY_DEFAULT)
        activity!!.registerReceiver(calibrationBroadcastReceiver, filter)

        return view
    }

    fun updateCalibrationStatusView() {
        data.refresh(camera)

        if (camera.processing) {
            calibrationStatus.text = "Processing..."
            calibrationButton.visibility = Button.GONE
            calibrationProgress.visibility = ProgressBar.VISIBLE
        } else {
            calibrationStatus.text = if (camera.calibratedJson.isEmpty()) "Not Calibrated" else "Calibrated"
            calibrationButton.visibility = Button.VISIBLE
            calibrationProgress.visibility = ProgressBar.GONE
        }
//        activity!!.unregisterReceiver(calibrationBroadcastReceiver)
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        fab?.startAnimation(AnimationUtils.loadAnimation(context, R.anim.grow))


    }

    override fun configureFab(fab: FloatingActionButton) {
        fab.imageResource = R.drawable.ic_add_white

        fab.setOnClickListener { v ->
            PhotoUtils.photoPicker(this@CameraCalibrationFragment, REQUEST_TAKE_PHOTO)
        }
    }

    lateinit var mCurrentPhotoPath: String


    override fun configureActionBar(actionBar: ActionBar) {
        actionBar.title = "Camera Calibration"
    }

    companion object {
        const val REQUEST_TAKE_PHOTO = 123

        fun newInstance(): CameraCalibrationFragment {
            val fragment = CameraCalibrationFragment()
            return fragment
        }
    }

    internal inner class CameraCalibrationHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        lateinit var image: ImageView

    }

    private inner class CameraCalibrationAdapter internal constructor() :
            QueryRecyclerAdapter<CameraCalibration, CameraCalibrationHolder>(Models.DEFAULT, CameraCalibration::class.java),
            View.OnClickListener, View.OnLongClickListener {


        override fun performQuery(): Result<CameraCalibration> {
            return (data select (CameraCalibration::class) where (CameraCalibration::camera eq camera)).get()
        }

        override fun onBindViewHolder(item: CameraCalibration, holder: CameraCalibrationHolder, position: Int) {
            Glide.with(this@CameraCalibrationFragment).load(item.getFileUrl()).into(holder.image)
            holder.itemView.tag = item
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CameraCalibrationHolder {
            val inflater = LayoutInflater.from(parent.context)
            val view = inflater.inflate(R.layout.item_camera_calibration, parent, false)
            val holder = CameraCalibrationHolder(view)
            holder.image = view.findViewById(R.id.cameraCalibrationImageView)
            view.setOnClickListener(this)
            view.setOnLongClickListener(this)
            return holder
        }

        override fun onClick(v: View) {

        }

        override fun onLongClick(v: View): Boolean {
            data.refresh(camera)
            if (camera.processing) {
                return false
            }

            val item = v.tag as CameraCalibration

            val builder = AlertDialog.Builder(context!!)

            builder.setTitle("Do you want to delete this item?")

            builder.setPositiveButton("Yes") { dialog, which ->
                data.delete(item)
                adapter.queryAsync()
                camera.calibratedJson = ""
                data.update(camera)
                updateCalibrationStatusView()

            }
            builder.setNegativeButton("Cancel") { dialog, which ->
            }

            val dialog = builder.create()
            dialog.show()

            return true
        }


    }

    val calibrationBroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            updateCalibrationStatusView()

        }
    }

}
