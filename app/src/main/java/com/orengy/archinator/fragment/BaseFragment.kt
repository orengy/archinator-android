package com.orengy.archinator.fragment

import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.support.v7.app.ActionBar
import com.orengy.archinator.ArchinatorApplication
import com.orengy.archinator.R
import io.requery.Persistable
import io.requery.kotlin.BlockingEntityStore
import org.jetbrains.anko.image

/**
 * Created by Adomas on 2017-11-03.
 */
abstract class BaseFragment : Fragment() {

    lateinit var data: BlockingEntityStore<Persistable>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        data = (context!!.applicationContext as ArchinatorApplication).data.toBlocking()
    }

    var fab: FloatingActionButton? = null
    var actionBar: ActionBar? = null

    fun addFab(fab: FloatingActionButton?): BaseFragment {
        if (fab == null)
            return this
        this.fab = fab
        configureFab(fab)
        return this
    }

    fun addActionBar(actionBar: ActionBar?): BaseFragment {
        if (actionBar == null)
            return this
        this.actionBar = actionBar
        configureActionBar(actionBar)
        return this
    }

    abstract fun configureFab(fab: FloatingActionButton)
    abstract fun configureActionBar(actionBar: ActionBar)
}