package com.orengy.archinator.fragment

import android.content.Context
import android.graphics.Color
import android.graphics.PorterDuff
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils

import com.orengy.archinator.R
import org.jetbrains.anko.imageResource
import org.jetbrains.anko.support.v4.toast
import android.support.v7.widget.ThemedSpinnerAdapter.Helper
import android.app.Activity
import android.content.Intent
import android.support.v7.app.ActionBar
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.TextView
import com.orengy.archinator.activity.ArrowFormActivity
import com.orengy.archinator.activity.RoundDetailsActivity
import com.orengy.archinator.activity.SessionFormActivity
import com.orengy.archinator.model.orm.*
import com.orengy.archinator.utils.Utils
import io.requery.android.QueryRecyclerAdapter
import io.requery.kotlin.eq
import io.requery.kotlin.refresh
import io.requery.query.Result
import org.jetbrains.anko.support.v4.startActivity
import org.jetbrains.anko.support.v4.startActivityForResult
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors


class SessionsFragment : BaseFragment() {

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == SESSION_FORM_REQUEST && resultCode == Activity.RESULT_OK) {
            adapter.queryAsync()
        }
    }

    private lateinit var adapter: SessionAdapter
    private lateinit var recyclerView: RecyclerView


    private var containerId: Int? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        containerId = container?.id
        val view = inflater!!.inflate(R.layout.fragment_sessions, container, false)

        adapter = SessionAdapter()
        adapter.setExecutor(Executors.newSingleThreadExecutor())

        recyclerView = view.findViewById(R.id.sessionRecyclerView)

        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(activity)

        adapter.queryAsync()


        return view
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)

        fab?.startAnimation(AnimationUtils.loadAnimation(context, R.anim.grow))
    }

    override fun configureFab(fab: FloatingActionButton) {
        fab.imageResource = R.drawable.ic_add_white

        fab.setOnClickListener { v ->
            startActivityForResult<SessionFormActivity>(SESSION_FORM_REQUEST)
        }
    }

    override fun configureActionBar(actionBar: ActionBar) {
        actionBar.title = "Sessions"
    }

    companion object {
        const val SESSION_FORM_REQUEST = 1234

        fun newInstance(): SessionsFragment {
            val fragment = SessionsFragment()
            return fragment
        }
    }

    internal inner class SessionHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        //        var image : ImageView? = null
        var name: TextView? = null
        var date: TextView? = null
    }

    private inner class SessionAdapter internal constructor() :
            QueryRecyclerAdapter<Session, SessionHolder>(Models.DEFAULT, Session::class.java),
            View.OnClickListener, View.OnLongClickListener {

        override fun onLongClick(v: View?): Boolean {
            val session = v!!.tag as Session
            val builder = AlertDialog.Builder(this@SessionsFragment.context!!)

            builder.setTitle("Do you want to delete this item?")

            builder.setPositiveButton("Yes") { dialog, which ->
                data.delete(session)
                this.queryAsync()
            }
            builder.setNegativeButton("Cancel") { dialog, which ->
            }

            val dialog = builder.create()
            dialog.show()

            return true
        }

        override fun performQuery(): Result<Session> {
            return (data select (Session::class)).get()
        }

        override fun onBindViewHolder(item: Session, holder: SessionHolder, position: Int) {
            holder.name!!.text = item.title
            holder.date!!.text = Utils.formatTime(item.createdAt)
            holder.itemView.tag = item
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SessionHolder {
            val inflater = LayoutInflater.from(parent.context)
            val view = inflater.inflate(R.layout.item_session, parent, false)
            val holder = SessionHolder(view)
            holder.name = view.findViewById<TextView>(R.id.sessionTitle)
            holder.date = view.findViewById<TextView>(R.id.sessionDate)
            view.setOnClickListener(this)
            view.setOnLongClickListener(this)
            return holder
        }

        override fun onClick(v: View) {
            val session = v.tag as Session? ?: return
//            data.refresh(session.rounds)
//            toast(session.title)

            val res = (data select (Round::class) where (Round::session eq session)).get().firstOrNull()
                    ?: return


            startActivity<RoundDetailsActivity>(RoundDetailsActivity.ARG_ROUND_ID to res.id)

        }
    }
}
