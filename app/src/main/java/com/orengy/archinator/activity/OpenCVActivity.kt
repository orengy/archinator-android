package com.orengy.archinator.activity

import android.app.Activity
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.orengy.archinator.R
import org.opencv.android.Utils
import org.opencv.imgproc.Imgproc
import android.provider.MediaStore
import android.content.Intent
import android.graphics.Bitmap
import kotlinx.android.synthetic.main.activity_open_cv.*
import java.nio.file.Files.delete
import org.opencv.objdetect.CascadeClassifier
import android.content.Context.MODE_PRIVATE
import org.opencv.android.LoaderCallbackInterface
import org.opencv.android.BaseLoaderCallback
import org.opencv.core.Mat
import org.opencv.core.Size
import org.opencv.core.Core
import org.opencv.core.Scalar
import java.nio.file.Files.delete
import android.content.Context.MODE_PRIVATE
import android.R.raw
import com.orengy.archinator.common.MatHelper
import org.jetbrains.anko.sdk25.coroutines.onClick
import org.jetbrains.anko.toast
import org.opencv.android.OpenCVLoader


class OpenCVActivity : BaseActivity() {

    companion object {
        const val REQUEST_IMAGE_CAPTURE = 1234
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_open_cv)

        if (!imageCaptured) {
            val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (takePictureIntent.resolveActivity(packageManager) != null) {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
            }
        }

        process_button.onClick {
            image_original.setImageBitmap(imageBitmap)
            val bmp32 = imageBitmap.copy(Bitmap.Config.ARGB_8888, true)
            val original = Mat()
            val temp = Mat()
            Utils.bitmapToMat(bmp32, original, true)

            Imgproc.cvtColor(original, temp, Imgproc.COLOR_RGBA2BGR)

            val closest = MatHelper.closestColors(temp)


//            val gray = Mat()
//            val blured = Mat()
//            Imgproc.cvtColor(original, gray, Imgproc.COLOR_BGR2GRAY)
//            Imgproc.blur(gray, blured, Size(3.0, 3.0))
//            val treshHold = threshold.text.toString().toDoubleOrNull() ?: 2.0
//            Imgproc.Canny(blured, blured, treshHold, treshHold * 3);

            val processedBmp: Bitmap = Bitmap.createBitmap(imageBitmap.width, imageBitmap.height, Bitmap.Config.ARGB_8888)
            Imgproc.cvtColor(closest, closest, Imgproc.COLOR_BGR2RGB)

            Utils.matToBitmap(closest, processedBmp)

            image_borders.setImageBitmap(processedBmp)
        }
    }

    lateinit var imageBitmap: Bitmap
    var imageCaptured = false
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            imageCaptured = true
            val extras = data.extras
            imageBitmap = extras!!.get("data") as Bitmap


        }
    }


    public override fun onResume() {
        super.onResume()
        OpenCVLoader.initDebug()
//        if (!OpenCVLoader.initDebug()) {
//            toast("Internal OpenCV library not found. Using OpenCV Manager for initialization")
//            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION, this, mLoaderCallback)
//        } else {
//            toast("OpenCV library found inside package. Using it!")
//            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS)
//        }
    }


    private val mLoaderCallback = object : BaseLoaderCallback(this) {
        override fun onManagerConnected(status: Int) {
            toast("Something")
            when (status) {
                LoaderCallbackInterface.SUCCESS -> {
                    if (!imageCaptured) {
                        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                        if (takePictureIntent.resolveActivity(packageManager) != null) {
                            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
                        }
                    }
                }
                else -> {
                    toast("Did not load properly")
                    super.onManagerConnected(status)
                }
            }
        }
    }
}
