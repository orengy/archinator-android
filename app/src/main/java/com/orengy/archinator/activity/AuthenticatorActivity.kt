package com.orengy.archinator.activity

import android.accounts.AccountAuthenticatorActivity
import android.app.Activity
import android.os.Bundle
import com.google.android.gms.common.api.GoogleApiClient
import com.orengy.archinator.R
import android.content.Intent
import android.content.ContentValues.TAG
import android.accounts.AccountManager
import android.accounts.Account
import android.content.ContentResolver
import com.orengy.archinator.utils.Constants
import com.orengy.archinator.utils.Constants.Auth.ARG_IS_ADDING_NEW_ACCOUNT


class AuthenticatorActivity : AccountAuthenticatorActivity() {

    private val REQ_LOGIN = 45235
    private lateinit var mAuthTokenType: String
    private lateinit var mAccountManager: AccountManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mAccountManager = AccountManager.get(baseContext);
        mAuthTokenType = intent.getStringExtra(Constants.Auth.ARG_AUTH_TYPE);

        val login = Intent(baseContext, LoginActivity::class.java)
        login.putExtras(intent.extras!!)
        startActivityForResult(login, REQ_LOGIN)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        if (requestCode == REQ_LOGIN && resultCode == Activity.RESULT_OK) {
            finishLogin(data)
        } else if (requestCode == REQ_LOGIN && resultCode == Activity.RESULT_CANCELED) {
            setAccountAuthenticatorResult(null)
            setResult(Activity.RESULT_OK, intent)
            finish()
        } else
            super.onActivityResult(requestCode, resultCode, data)
    }

    private fun finishLogin(intent: Intent) {
        val accountName = intent.getStringExtra(AccountManager.KEY_ACCOUNT_NAME)
        val accountType = intent.getStringExtra(AccountManager.KEY_ACCOUNT_TYPE)
        val authToken = intent.getStringExtra(AccountManager.KEY_AUTHTOKEN)
        val account = Account(accountName, accountType)

        if (getIntent().getBooleanExtra(ARG_IS_ADDING_NEW_ACCOUNT, false)) {
            mAccountManager.addAccountExplicitly(account, null, intent.getBundleExtra(AccountManager.KEY_USERDATA))

            ContentResolver.setSyncAutomatically(account, Constants.Sync.AUTHORITY, true)

            ContentResolver.addPeriodicSync(
                    account,
                    Constants.Sync.AUTHORITY,
                    Bundle.EMPTY,
                    Constants.Sync.SYNC_INTERVAL)

//            val settingsBundle = Bundle()
//            settingsBundle.putBoolean(
//                    ContentResolver.SYNC_EXTRAS_MANUAL, true)
//            settingsBundle.putBoolean(
//                    ContentResolver.SYNC_EXTRAS_EXPEDITED, true)

//            ContentResolver.requestSync(account, Constants.Sync.AUTHORITY, settingsBundle)

        }

        mAccountManager.setAuthToken(account, mAuthTokenType, authToken)

        setAccountAuthenticatorResult(intent.extras)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }
}
