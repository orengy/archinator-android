package com.orengy.archinator.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.orengy.archinator.ArchinatorApplication
import io.requery.kotlin.BlockingEntityStore
import io.requery.Persistable
import io.requery.reactivex.KotlinReactiveEntityStore

/**
 * Created by Adomas on 2017-10-25.
 */
abstract class BaseActivity : AppCompatActivity() {
    lateinit var data: BlockingEntityStore<Persistable>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        data = (application as ArchinatorApplication).data.toBlocking()
    }
}