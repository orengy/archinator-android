package com.orengy.archinator.activity

import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.graphics.Canvas
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import com.orengy.archinator.R
import com.orengy.archinator.model.orm.*
import io.requery.android.QueryRecyclerAdapter
import io.requery.kotlin.eq
import io.requery.query.Result
import kotlinx.android.synthetic.main.activity_end_form.*
import java.util.concurrent.Executors
import android.content.Intent
import com.orengy.archinator.service.ArrowProcessingService
import android.content.IntentFilter
import android.content.res.ColorStateList
import android.graphics.Paint
import android.net.Uri
import android.support.v4.content.ContextCompat
import android.support.v4.widget.ImageViewCompat
import android.support.v7.app.AlertDialog
import android.view.*
import com.bumptech.glide.Glide
import com.orengy.archinator.utils.PhotoUtils
import org.jetbrains.anko.textColor
import kotlin.math.roundToInt


class EndFormActivity : BaseActivity(), View.OnTouchListener {


    companion object {
        const val ARG_PHOTO_URI = "photo_uri"
        const val ARG_END_ID = "end_id"
        const val ARG_ROUND_ID = "round_id"

        const val CAPTURE_PHOTO_REQUEST = 4394
    }

    lateinit var endEntity: End
    private lateinit var adapter: EndMediaAdapter
    private lateinit var arrowAdapter: EndArrowAdapter
    lateinit var arrow: EquipmentArrow

    var selectedArrow = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_end_form)

        val photoUri = intent.extras?.getString(ARG_PHOTO_URI, "") ?: ""
        val endId = intent.extras?.getInt(ARG_END_ID, 0) ?: 0
        val roundId = intent.extras?.getInt(ARG_ROUND_ID, 0) ?: 0

        if ((photoUri.isEmpty() || roundId == 0) && endId == 0) {
            finish()
            return
        }

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_close_white)
        supportActionBar?.title =
                if (endId == 0)
                    getString(R.string.title_activity_end_form_add)
                else
                    getString(R.string.title_activity_end_form_edit)


        val end = data.findByKey(End::class, endId)


        if (end != null) {
            endEntity = end
            data.refresh(endEntity.round)
        } else {
            val round = data.findByKey(Round::class, roundId)
            if (round == null) {
                finish()
                return
            }

            endEntity = EndEntity()
            endEntity.round = round
//            endEntity.createdAt = System.currentTimeMillis() / 1000
            data.insert(endEntity)

            val endMediaEntity = EndMediaEntity()
            endMediaEntity.localFileUrl = photoUri
            endMediaEntity.end = endEntity
            endMediaEntity.processing = true
            endMediaEntity.detectedArrows = 0
            data.insert(endMediaEntity)

            val msgIntent = Intent(this, ArrowProcessingService::class.java)
            msgIntent.putExtra(ArrowProcessingService.ARG_END_MEDIA_ID, endMediaEntity.id)
            startService(msgIntent)
        }

        data.refresh(endEntity.round)
        data.refresh(endEntity.round.session)
        arrow = endEntity.round.session.equipmentArrow


        fetchArrows()

        endTargetSurfaceView.holder.addCallback(surfaceViewCallback)
        endTargetSurfaceView.setWillNotDraw(false)
        endTargetSurfaceView.setOnTouchListener(this)

        floatingActionButton.setOnClickListener {
            currentSurfaceViewState = SurfaceViewState.INSERTING
            selectedArrow = 0
            tryDrawing(endTargetSurfaceView.holder)
        }

        adapter = EndMediaAdapter()
        adapter.setExecutor(Executors.newSingleThreadExecutor())
        endMediaRecyclerView.adapter = adapter
        endMediaRecyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)

        adapter.queryAsync()

        arrowAdapter = EndArrowAdapter()
        arrowAdapter.setExecutor(Executors.newSingleThreadExecutor())
        endArrowRecyclerView.adapter = arrowAdapter
        endArrowRecyclerView.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        arrowAdapter.queryAsync()


        val filter = IntentFilter(ArrowProcessingService.ACTION_RESP)
        filter.addCategory(Intent.CATEGORY_DEFAULT)
        val receiver = ArrowProcessingBroadcastReceiver()
        registerReceiver(receiver, filter)
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_OK, Intent())
        finish()
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home,
            R.id.action_save -> {
                setResult(Activity.RESULT_OK, Intent())
                finish()
                return true
            }
            R.id.action_target_photo -> {
                PhotoUtils.photoPicker(this, CAPTURE_PHOTO_REQUEST)
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }


    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val photoUrl = PhotoUtils.handleResponse(this, requestCode, resultCode, data)

        if (photoUrl != null) {
            val endMediaEntity = EndMediaEntity()
            endMediaEntity.localFileUrl = photoUrl
            endMediaEntity.end = endEntity
            endMediaEntity.processing = true
            endMediaEntity.detectedArrows = 0
            this.data.insert(endMediaEntity)

            val msgIntent = Intent(this, ArrowProcessingService::class.java)
            msgIntent.putExtra(ArrowProcessingService.ARG_END_MEDIA_ID, endMediaEntity.id)
            startService(msgIntent)
            adapter.queryAsync()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.end_form, menu)
        return true
    }


    inner class ArrowProcessingBroadcastReceiver : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            adapter.queryAsync()
            arrowAdapter.queryAsync()
            tryDrawing(endTargetSurfaceView.holder)

        }
    }

    internal inner class EndMediaHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var image: ImageView? = null
        var progressBar: ProgressBar? = null
        var detectedArrowsTextView: TextView? = null
    }

    private inner class EndMediaAdapter internal constructor() :
            QueryRecyclerAdapter<EndMedia, EndMediaHolder>(Models.DEFAULT, EndMedia::class.java),
            View.OnClickListener {

        override fun onClick(itemView: View?) {

        }


        override fun performQuery(): Result<EndMedia> {
            return (data select (EndMedia::class) where (EndMedia::end eq endEntity)).get()
        }

        override fun onBindViewHolder(item: EndMedia, holder: EndMediaHolder, position: Int) {
            holder.detectedArrowsTextView!!.visibility = if (item.processing) TextView.GONE else TextView.VISIBLE
            holder.progressBar!!.visibility = if (!item.processing) ProgressBar.GONE else ProgressBar.VISIBLE
            holder.detectedArrowsTextView!!.text = "${item.detectedArrows} arrow${if (item.detectedArrows == 1) "" else "s"} detected"
            Glide.with(this@EndFormActivity).load(item.getFileUrl()).into(holder.image!!)
            holder.itemView.tag = item
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EndMediaHolder {
            val inflater = LayoutInflater.from(parent.context)
            val view = inflater.inflate(R.layout.item_end_media, parent, false)
            val holder = EndMediaHolder(view)
            holder.detectedArrowsTextView = view.findViewById(R.id.endMediaDetectedArrowsTextView)
            holder.image = view.findViewById(R.id.endMediaImage)
            holder.progressBar = view.findViewById(R.id.endMediaProgressBar)
            view.setOnClickListener(this)
            return holder
        }
    }


    internal inner class EndArrowHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        lateinit var bg: ImageView
        lateinit var score: TextView
    }


    private inner class EndArrowAdapter internal constructor() :
            QueryRecyclerAdapter<EndArrow, EndArrowHolder>(Models.DEFAULT, EndArrow::class.java),
            View.OnClickListener,
            View.OnLongClickListener {

        override fun onLongClick(itemView: View): Boolean {
            val item = itemView.tag as EndArrow

            val builder = AlertDialog.Builder(this@EndFormActivity)

            builder.setTitle("Do you want to delete this item?")

            builder.setPositiveButton("Yes") { dialog, which ->
                data.delete(item)
                this.queryAsync()
                selectedArrow = 0
                currentSurfaceViewState = SurfaceViewState.VIEWING
                tryDrawing(endTargetSurfaceView.holder)
            }
            builder.setNegativeButton("Cancel") { dialog, which ->
            }

            val dialog = builder.create()
            dialog.show()

            return true
        }

        override fun onClick(itemView: View) {
            val item = itemView.tag as EndArrow
            selectedArrow = item.id
            currentSurfaceViewState = SurfaceViewState.EDITING
            tryDrawing(endTargetSurfaceView.holder)
        }


        override fun performQuery(): Result<EndArrow> {
            fetchArrows()
            return arrows
        }

        override fun onBindViewHolder(item: EndArrow, holder: EndArrowHolder, position: Int) {

            ImageViewCompat.setImageTintList(holder.bg,
                    ColorStateList.valueOf(ContextCompat.getColor(this@EndFormActivity, when ((item.score - 1) / 2) {
                        0 -> R.color.white
                        1 -> R.color.black
                        2 -> R.color.blue
                        3 -> R.color.red
                        4 -> R.color.yellow
                        else -> R.color.black
                    })))

            holder.score.textColor = ContextCompat.getColor(this@EndFormActivity, when (item.score) {
                in 1..2, in 9..10 -> R.color.black
                else -> R.color.white
            })



            holder.score.text = if (item.score > 0) item.score.toString() else "M"
            holder.itemView.tag = item
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EndArrowHolder {
            val inflater = LayoutInflater.from(parent.context)
            val view = inflater.inflate(R.layout.item_end_arrow, parent, false)
            val holder = EndArrowHolder(view)

            holder.bg = view.findViewById(R.id.scoreBg)
            holder.score = view.findViewById(R.id.scoreTextView)
            view.setOnClickListener(this)
            view.setOnLongClickListener(this)
            return holder
        }
    }

    val surfaceViewCallback = object : SurfaceHolder.Callback {
        override fun surfaceCreated(holder: SurfaceHolder) {

            tryDrawing(holder)
        }

        override fun surfaceChanged(holder: SurfaceHolder, frmt: Int, w: Int, h: Int) {
            tryDrawing(holder)
        }

        override fun surfaceDestroyed(holder: SurfaceHolder) {}


    }

    private fun tryDrawing(holder: SurfaceHolder,
                           zoom: Int = 1,
                           refX: Float = 0f,
                           refY: Float = 0f,
                           x: Float = 0f,
                           y: Float = 0f) {
        val canvas = holder.lockCanvas()
        if (canvas != null) {
            drawMyStuff(canvas, zoom, refX, refY, x, y)
            holder.unlockCanvasAndPost(canvas)
        }
    }

    var surfaceViewLocation = intArrayOf(0, 0)
    var surfaceViewDownX = 0f
    var surfaceViewDownY = 0f

    var currentSurfaceViewState = SurfaceViewState.VIEWING

    enum class SurfaceViewState {
        VIEWING,
        INSERTING,
        EDITING
    }

    var editingArrowX = 0.0
    var editingArrowY = 0.0
    var editingArrowScore = 0

    lateinit var arrows: Result<EndArrow>

    fun fetchArrows() {
        arrows = (data select (EndArrow::class) where (EndArrow::end eq endEntity)).get()
    }

    override fun onTouch(v: View, event: MotionEvent): Boolean {
        when (event.action and MotionEvent.ACTION_MASK) {


            MotionEvent.ACTION_MOVE -> {
                tryDrawing(endTargetSurfaceView.holder, 3,
                        surfaceViewDownX,
                        surfaceViewDownY,
                        event.rawX - surfaceViewLocation[0],
                        event.rawY - surfaceViewLocation[1]
                )
            }
            MotionEvent.ACTION_DOWN -> {
                if (currentSurfaceViewState == SurfaceViewState.INSERTING) {
                    val endArrow = EndArrowEntity()
                    endArrow.end = endEntity
                    endArrow.posX = 0.0
                    endArrow.posY = 0.0
                    endArrow.score = 0
                    data.insert(endArrow)
                    selectedArrow = endArrow.id
                    fetchArrows()
                }

                endTargetSurfaceView.getLocationOnScreen(surfaceViewLocation)
                surfaceViewDownX = event.rawX - surfaceViewLocation[0]
                surfaceViewDownY = event.rawY - surfaceViewLocation[1]
                tryDrawing(endTargetSurfaceView.holder, 3,
                        surfaceViewDownX,
                        surfaceViewDownY,
                        event.rawX - surfaceViewLocation[0],
                        event.rawY - surfaceViewLocation[1]
                )
            }
            MotionEvent.ACTION_UP -> {
                if (currentSurfaceViewState != SurfaceViewState.VIEWING) {
                    //TODO: UPDATE arrow position
                    val editingArrow = data.findByKey(EndArrow::class, selectedArrow)!!
                    editingArrow.posX = editingArrowX
                    editingArrow.posY = editingArrowY
                    editingArrow.score = editingArrowScore
                    data.update(editingArrow)
                    fetchArrows()
                    arrowAdapter.queryAsync()
                    selectedArrow = 0
                    currentSurfaceViewState = SurfaceViewState.VIEWING
                }

                tryDrawing(endTargetSurfaceView.holder)
            }
        }

        return true
    }

    private fun drawMyStuff(canvas: Canvas, zoom: Int, refX: Float, refY: Float, x: Float, y: Float) {
        canvas.drawColor(resources.getColor(R.color.bg))
        val target = resources.getDrawable(R.drawable.ic_archery_target_wa)
        val sensitivity = 3.0
        val padding = 50

        val size = (Math.min(canvas.width, canvas.height) - padding) * zoom

        val tcx = (((canvas.width / 2 - refX) * zoom) + refX + (refX - x) * sensitivity).roundToInt()
        val tcy = (((canvas.height / 2 - refY) * zoom) + refY + (refY - y) * sensitivity).roundToInt() -
                if (zoom > 1) 300 else 0

        target.setBounds(
                tcx - size / 2,
                tcy - size / 2,
                tcx + size / 2,
                tcy + size / 2
        )
        target.draw(canvas)

        val arrowPaint = Paint(Paint.ANTI_ALIAS_FLAG)
        arrowPaint.style = Paint.Style.FILL
        arrowPaint.color = resources.getColor(R.color.white)

        val arrowPaintSelected = Paint(Paint.ANTI_ALIAS_FLAG)
        arrowPaintSelected.style = Paint.Style.FILL
        arrowPaintSelected.color = resources.getColor(R.color.primary)

        val arrowPaintStroke = Paint(Paint.ANTI_ALIAS_FLAG)
        arrowPaintStroke.style = Paint.Style.STROKE
        arrowPaintStroke.color = resources.getColor(R.color.black)
        arrowPaintStroke.strokeWidth = 4f

        val arrowSize = arrow.diameterInMm * size / (0.6 * 1000)

        val scorePaint = Paint(Paint.ANTI_ALIAS_FLAG)
        scorePaint.style = Paint.Style.FILL
        scorePaint.color = resources.getColor(R.color.black)
        scorePaint.textAlign = Paint.Align.CENTER
        scorePaint.textSize = 2 * arrowSize.toFloat() * 0.7f



        for (arrow in arrows) {

            if (zoom > 1 && arrow.id == selectedArrow) {
                canvas.drawCircle(
                        x,
                        y - 300,
                        arrowSize.toFloat(),
                        arrowPaintSelected
                )

                canvas.drawCircle(
                        x,
                        y - 300,
                        arrowSize.toFloat(),
                        arrowPaintStroke
                )

                editingArrowX = (x - tcx) * 2 * 0.6 / size
                editingArrowY = (y - 300 - tcy) * 2 * 0.6 / size

                val distance = Math.sqrt(
                        Math.pow((x - tcx).toDouble(), 2.0) +
                                Math.pow((y - 300 - tcy).toDouble(), 2.0)
                ) - arrowSize

                val s = 10 - Math.floor(distance * 20 / size).toInt()

                editingArrowScore = when {
                    s < 1 -> 0
                    s in 1..10 -> s
                    else -> 10
                }

                val score = when {
                    s in 1..10 -> "$s"
                    s < 1 -> "M"
                    else -> "10"
                }
                canvas.drawText(score, x, y - 300 + scorePaint.textSize * 0.4f, scorePaint)
            } else {
                canvas.drawCircle(
                        (tcx + arrow.posX * size / (2 * 0.6)).toFloat(),
                        (tcy + arrow.posY * size / (2 * 0.6)).toFloat(),
                        arrowSize.toFloat(),
                        if (arrow.id == selectedArrow) arrowPaintSelected else arrowPaint
                )

                canvas.drawCircle(
                        (tcx + arrow.posX * size / (2 * 0.6)).toFloat(),
                        (tcy + arrow.posY * size / (2 * 0.6)).toFloat(),
                        arrowSize.toFloat(),
                        arrowPaintStroke
                )
            }


        }
    }
}
