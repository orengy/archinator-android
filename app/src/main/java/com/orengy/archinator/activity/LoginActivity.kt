package com.orengy.archinator.activity

import android.app.Activity
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.google.android.gms.auth.api.Auth
import com.orengy.archinator.R
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.auth.api.Auth.GOOGLE_SIGN_IN_API
import com.google.android.gms.common.api.GoogleApiClient
import org.jetbrains.anko.find
import org.jetbrains.anko.sdk25.coroutines.onClick
import android.content.Intent
import com.google.android.gms.auth.api.signin.GoogleSignInResult
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.common.ConnectionResult
import com.orengy.archinator.model.request.AuthRequest
import com.orengy.archinator.model.request.BaseRequest
import com.orengy.archinator.model.response.AuthLoginResponseModel
import com.orengy.archinator.utils.Constants
import org.jetbrains.anko.toast
import android.accounts.AccountManager.KEY_ERROR_MESSAGE
import android.accounts.AccountManager
import android.R.attr.name
import android.widget.ImageView


class LoginActivity : BaseActivity(), GoogleApiClient.OnConnectionFailedListener {

    private lateinit var mGoogleApiClient: GoogleApiClient
    private val RC_SIGN_IN = 9001
    private lateinit var mAccountType: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        mAccountType = intent.getStringExtra(Constants.Auth.ARG_ACCOUNT_TYPE);

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .requestIdToken(Constants.Auth.GOOGLE_APPS_SERVER_CLIENT_ID)
                .build()

        mGoogleApiClient = GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build()

        find<ImageView>(R.id.button_sign_in_google).onClick {
            val signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient)
            startActivityForResult(signInIntent, RC_SIGN_IN)
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
            if (result.isSuccess) {
                val acct = result.signInAccount

                AuthRequest(this).login(
                        Constants.Auth.AUTHENTICATION_METHOD_GOOGLE,
                        acct?.idToken ?: "",
                        acct?.email ?: "",
                        object : BaseRequest.BaseResponse<AuthLoginResponseModel> {
                            override fun onSuccess(data: AuthLoginResponseModel) {
                                finishLogin(data)
                            }
                            override fun onFail(message: String) {}
                        }
                ).addLoadDialog("Logging in")
            } else {
                toast("Google authentication failed")
            }
        }
    }

    fun finishLogin(loginResponse: AuthLoginResponseModel) {
        val data = Bundle()

        data.putString(AccountManager.KEY_ACCOUNT_NAME, loginResponse.email)
        data.putString(AccountManager.KEY_ACCOUNT_TYPE, mAccountType)
        data.putString(AccountManager.KEY_AUTHTOKEN, loginResponse.accessToken)

        val userData = Bundle()
        userData.putString(Constants.Auth.ARG_USER_EXTRA_DATA_NAME, loginResponse.name)
        userData.putString(Constants.Auth.ARG_USER_EXTRA_DATA_UID, loginResponse.uid)
        userData.putString(Constants.Auth.ARG_USER_EXTRA_DATA_EMAIL, loginResponse.email)
        data.putBundle(AccountManager.KEY_USERDATA, userData)

        val result = Intent()
        result.putExtras(data)
        setResult(RESULT_OK, result);
        finish();
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        toast("failed to connect google services")
    }

    override fun onBackPressed() {
        val result = Intent()
        val b = Bundle()
        result.putExtras(b)

//        setAccountAuthenticatorResult(null) // null means the user cancelled the authorization processs
        setResult(Activity.RESULT_CANCELED, result)
        finish()

//        setResult(Activity.RESULT_CANCELED)
//        finish()
//        super.onBackPressed()
    }
}
