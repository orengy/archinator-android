package com.orengy.archinator.activity

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.jaredrummler.android.colorpicker.ColorPickerDialog
import com.jaredrummler.android.colorpicker.ColorPickerDialogListener
import com.jaredrummler.android.colorpicker.ColorShape
import com.orengy.archinator.ArchinatorApplication
import com.orengy.archinator.R
import com.orengy.archinator.fragment.ArrowsFragment
import com.orengy.archinator.model.orm.Color

import com.orengy.archinator.model.orm.EquipmentArrow
import com.orengy.archinator.model.orm.EquipmentArrowEntity
import com.orengy.archinator.model.orm.base.Record
import kotlinx.android.synthetic.main.activity_arrow_form.*
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.sdk25.coroutines.onClick

class ArrowFormActivity : BaseActivity(), ColorPickerDialogListener {

    companion object {
        val ARG_ARROW_ID = "arrow_id"
    }

    override fun onDialogDismissed(dialogId: Int) {}

    lateinit var colors: List<Color>
    lateinit var colorsMap: Map<Int, Color>
    lateinit var colorsIntArray: IntArray

    lateinit var arrowEntity: EquipmentArrow

    override fun onColorSelected(dialogId: Int, color: Int) {
        when (dialogId) {
            R.id.arrowFletchingOneColor -> {
                arrowEntity.fletchingOneColor = colorsMap[color]!!
                arrowFletchingOneColor.backgroundColor = addAlpha(arrowEntity.fletchingOneColor.hex)
            }
            R.id.arrowFletchingTwoColor -> {
                arrowEntity.fletchingTwoColor = colorsMap[color]!!
                arrowFletchingTwoColor.backgroundColor = addAlpha(arrowEntity.fletchingTwoColor.hex)
            }
            R.id.arrowFletchingThreeColor -> {
                arrowEntity.fletchingThreeColor = colorsMap[color]!!
                arrowFletchingThreeColor.backgroundColor = addAlpha(arrowEntity.fletchingThreeColor.hex)
            }
            R.id.arrowNockColor -> {
                arrowEntity.nockColor = colorsMap[color]!!
                arrowNockColor.backgroundColor = addAlpha(arrowEntity.nockColor.hex)
            }
            R.id.arrowShaftColor -> {
                arrowEntity.shaftColor = colorsMap[color]!!
                arrowShaftColor.backgroundColor = addAlpha(arrowEntity.shaftColor.hex)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_arrow_form)

        val arrowId = intent.extras?.getInt(ARG_ARROW_ID, 0) ?: 0

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_close_white);
        supportActionBar?.title =
                if (arrowId == 0)
                    getString(R.string.title_activity_arrow_form_add)
                else
                    getString(R.string.title_activity_arrow_form_edit)



        colors = data.select(Color::class).get().toList()

        colorsMap = colors.associateBy { addAlpha(it.hex) }

        colorsIntArray = colors.map { addAlpha(it.hex) }.toIntArray()


        val arrow = data.findByKey(EquipmentArrow::class, arrowId)
        if (arrow != null) {
            arrowEntity = arrow
            data.refresh(arrowEntity.fletchingOneColor)
            data.refresh(arrowEntity.fletchingTwoColor)
            data.refresh(arrowEntity.fletchingThreeColor)
            data.refresh(arrowEntity.nockColor)
            data.refresh(arrowEntity.shaftColor)
        } else {
            arrowEntity = EquipmentArrowEntity()
            arrowEntity.fletchingOneColor = colors.first()
            arrowEntity.fletchingTwoColor = colors.first()
            arrowEntity.fletchingThreeColor = colors.first()
            arrowEntity.nockColor = colors.first()
            arrowEntity.shaftColor = colors.first()

        }
        loadDataToViews()

        arrowFletchingOneColor.onClick {
            showColorPicker(R.id.arrowFletchingOneColor, addAlpha(arrowEntity.fletchingOneColor.hex))
        }
        arrowFletchingTwoColor.onClick {
            showColorPicker(R.id.arrowFletchingTwoColor, addAlpha(arrowEntity.fletchingTwoColor.hex))
        }
        arrowFletchingThreeColor.onClick {
            showColorPicker(R.id.arrowFletchingThreeColor, addAlpha(arrowEntity.fletchingThreeColor.hex))
        }
        arrowNockColor.onClick {
            showColorPicker(R.id.arrowNockColor, addAlpha(arrowEntity.nockColor.hex))
        }
        arrowShaftColor.onClick {
            showColorPicker(R.id.arrowShaftColor, addAlpha(arrowEntity.shaftColor.hex))
        }
    }

    fun showColorPicker(dialogId: Int, color: Int) {
        ColorPickerDialog.newBuilder()
                .setPresets(
                        colorsIntArray)
                .setColorShape(ColorShape.SQUARE)
                .setAllowCustom(false)
                .setShowAlphaSlider(false)
                .setShowColorShades(false)
                .setColor(color)
                .setDialogId(dialogId)
                .show(this);
    }

    fun loadDataToViews() {
        arrowName.setText(arrowEntity.name)
        arrowDiameter.setText(arrowEntity.diameterInMm.toString())
        arrowFletchingOneColor.backgroundColor = addAlpha(arrowEntity.fletchingOneColor.hex)
        arrowFletchingTwoColor.backgroundColor = addAlpha(arrowEntity.fletchingTwoColor.hex)
        arrowFletchingThreeColor.backgroundColor = addAlpha(arrowEntity.fletchingThreeColor.hex)
        arrowNockColor.backgroundColor = addAlpha(arrowEntity.nockColor.hex)
        arrowShaftColor.backgroundColor = addAlpha(arrowEntity.shaftColor.hex)
    }

    fun addAlpha(color: Int): Int {
        return (255 shl 24) or color
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
            R.id.action_save -> {
                arrowEntity.name = arrowName.text.toString()
                arrowEntity.diameterInMm = arrowDiameter.text.toString().toDoubleOrNull() ?: 0.0
                data.upsert(arrowEntity)

                setResult(Activity.RESULT_OK, Intent())
                finish()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.arrow_form, menu)
        return true
    }

}
