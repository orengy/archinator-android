package com.orengy.archinator.activity

import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.view.Menu
import android.view.MenuItem
import android.widget.TextView
import com.orengy.archinator.ArchinatorApplication
import com.orengy.archinator.R
import com.orengy.archinator.fragment.ArrowsFragment
import com.orengy.archinator.fragment.CameraCalibrationFragment
import com.orengy.archinator.fragment.SessionsFragment
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import org.jetbrains.anko.find
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

class MainActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
        nav_view.setCheckedItem(R.id.nav_sessions)

        nav_view.getHeaderView(0).find<TextView>(R.id.nav_header_text_view_name).text = (application as ArchinatorApplication).userName
        nav_view.getHeaderView(0).find<TextView>(R.id.nav_header_text_view_email).text = (application as ArchinatorApplication).userEmail

        supportFragmentManager.beginTransaction()
                .add(R.id.fragment_container_main, SessionsFragment.newInstance().addFab(fab).addActionBar(supportActionBar)).commit();


    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.nav_sessions -> {
                supportFragmentManager.beginTransaction()
                        .replace(R.id.fragment_container_main, SessionsFragment.newInstance().addFab(fab).addActionBar(supportActionBar)).commit();
            }
            R.id.nav_analytics -> {
                startActivity<OpenCVActivity>()
            }
            R.id.nav_arrows -> {
                supportFragmentManager.beginTransaction()
                        .replace(R.id.fragment_container_main, ArrowsFragment.newInstance().addFab(fab).addActionBar(supportActionBar)).commit();
            }
            R.id.nav_camera_calibration -> {
                supportFragmentManager.beginTransaction()
                        .replace(R.id.fragment_container_main, CameraCalibrationFragment.newInstance().addFab(fab).addActionBar(supportActionBar)).commit();
            }
            R.id.nav_settings -> {
                startActivity<MlMnistActivity>()
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
}
