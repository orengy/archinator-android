package com.orengy.archinator.activity

import android.app.Activity
import android.os.Bundle
import com.orengy.archinator.R
import kotlinx.android.synthetic.main.activity_round_details.*
import org.jetbrains.anko.sdk25.coroutines.onClick
import android.content.Intent
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.orengy.archinator.model.orm.*
import io.requery.android.QueryRecyclerAdapter
import io.requery.kotlin.eq
import io.requery.query.Result
import org.jetbrains.anko.startActivityForResult
import java.util.concurrent.Executors
import android.support.v7.app.AlertDialog
import android.view.MenuItem
import com.orengy.archinator.utils.PhotoUtils



class RoundDetailsActivity : BaseActivity() {

    companion object {
        val ARG_ROUND_ID = "round"
        const val PICK_IMAGE = 124
        const val END_FORM_REQUEST = 1234
    }

    lateinit var round: Round
    private lateinit var adapter: EndAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_round_details)

        val roundId = intent.extras?.getInt(ARG_ROUND_ID, 0) ?: 0

        if (roundId == 0)
            finish()

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.title_activity_round_details)

        val r = data.findByKey(Round::class, roundId)
        if (r == null) {
            finish()
            return
        } else {
            round = r
        }

        floatingActionButton.onClick {
            PhotoUtils.photoPicker(this@RoundDetailsActivity, PICK_IMAGE)
        }

        adapter = EndAdapter()
        adapter.setExecutor(Executors.newSingleThreadExecutor())
        endRecyclerView.layoutManager = LinearLayoutManager(this)

        endRecyclerView.adapter = adapter
        adapter.queryAsync()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }


    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val photoUrl = PhotoUtils.handleResponse(this, requestCode, resultCode, data)

        if (photoUrl != null) {

            startActivityForResult<EndFormActivity>(
                    END_FORM_REQUEST,
                    EndFormActivity.ARG_PHOTO_URI to photoUrl,
                    EndFormActivity.ARG_ROUND_ID to round.id
            )
        }
        if (requestCode == END_FORM_REQUEST && resultCode == Activity.RESULT_OK) {
            adapter.queryAsync()
        }
    }

    internal inner class EndHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        lateinit var text: TextView

    }

    private inner class EndAdapter internal constructor() :
            QueryRecyclerAdapter<End, EndHolder>(Models.DEFAULT, End::class.java),
            View.OnClickListener, View.OnLongClickListener {

        override fun onLongClick(v: View?): Boolean {
            val end = v!!.tag as End
            val builder = AlertDialog.Builder(this@RoundDetailsActivity)

            builder.setTitle("Do you want to delete this item?")

            builder.setPositiveButton("Yes") { dialog, which ->
                data.delete(end)
                this.queryAsync()
            }
            builder.setNegativeButton("Cancel") { dialog, which ->
            }

            val dialog = builder.create()
            dialog.show()

            return true
        }

        override fun onClick(itemView: View?) {
            val end = itemView!!.tag as End

            startActivityForResult<EndFormActivity>(
                    END_FORM_REQUEST,
                    EndFormActivity.ARG_END_ID to end.id
            )
        }


        override fun performQuery(): Result<End> {
            return (data select (End::class) where (End::round eq round)).get()
        }

        override fun onBindViewHolder(item: End, holder: EndHolder, position: Int) {
            val arrows = (data select (EndArrow::class) where (EndArrow::end eq item)).get()
            holder.text.text = arrows.joinToString(separator = ", ") { it.score.toString() }
            holder.itemView.tag = item
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EndHolder {
            val inflater = LayoutInflater.from(parent.context)
            val view = inflater.inflate(R.layout.item_end, parent, false)
            val holder = EndHolder(view)
            holder.text = view.findViewById(R.id.endScoresTextView)
            view.setOnClickListener(this)
            view.setOnLongClickListener(this)
            return holder
        }
    }


}
