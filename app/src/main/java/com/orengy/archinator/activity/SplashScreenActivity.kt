package com.orengy.archinator.activity

import android.os.Bundle
import com.orengy.archinator.R
import com.orengy.archinator.utils.UserUtils
import android.accounts.Account
import android.accounts.AccountManager
import android.accounts.AccountManagerCallback
import android.accounts.AccountManagerFuture
import android.content.Context
import com.orengy.archinator.ArchinatorApplication
import com.orengy.archinator.utils.Constants
import org.jetbrains.anko.startActivity


class SplashScreenActivity : BaseActivity() {

//    private lateinit var data: KotlinReactiveEntityStore<Persistable>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)
        supportActionBar?.hide()

        val manager = getSystemService(Context.ACCOUNT_SERVICE) as AccountManager

        manager.getAuthTokenByFeatures(
                Constants.Auth.ACCOUNT_TYPE,
                Constants.Auth.AUTH_TOKEN_TYPE_FULL_ACCESS,
                null,
                this, null, null,
                { future ->
                    if (!future.isCancelled) {
                        val bnd: Bundle = future.result
                        val accountName = bnd.getString(AccountManager.KEY_ACCOUNT_NAME)
                        (application as ArchinatorApplication).user = Account(accountName, Constants.Auth.ACCOUNT_TYPE)
                        startActivity<MainActivity>()
                    }
                    finish()
                }, null)
    }
}
