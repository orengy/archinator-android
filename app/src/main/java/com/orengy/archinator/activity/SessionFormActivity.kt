package com.orengy.archinator.activity

import android.app.Activity
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.*
import android.widget.ArrayAdapter
import android.widget.TextView
import com.orengy.archinator.R
import android.widget.Toast
import com.bumptech.glide.Glide
import com.orengy.archinator.fragment.ArrowsFragment
import com.orengy.archinator.model.orm.*
import com.orengy.archinator.utils.PhotoUtils
import com.orengy.archinator.utils.Utils
import io.requery.android.QueryAdapter
import io.requery.kotlin.eq
import io.requery.query.Result
import kotlinx.android.synthetic.main.activity_arrow_form.*
import kotlinx.android.synthetic.main.activity_session_form.*
import org.jetbrains.anko.backgroundColor
import org.jetbrains.anko.startActivityForResult
import java.util.concurrent.Executors


class SessionFormActivity : BaseActivity() {

    companion object {
        val ARG_SESSION_ID = "session_id"
        val PICK_IMAGE = 5732
    }

    lateinit var sessionEntity: Session
    var roundEntity: Round? = null
    private lateinit var adapter: ArrowAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_session_form)

        val sessionId = intent.extras?.getInt(SessionFormActivity.ARG_SESSION_ID, 0) ?: 0

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_close_white);
        supportActionBar?.title =
                if (sessionId == 0)
                    getString(R.string.title_activity_session_form_add)
                else
                    getString(R.string.title_activity_session_form_edit)

        val session = data.findByKey(Session::class, sessionId)
        if (session != null) {
            sessionEntity = session
//            data.refresh(sessionEntity.rounds)
        } else {
            sessionEntity = SessionEntity()
            sessionEntity.createdAt = System.currentTimeMillis()
            sessionEntity.title = "Trainings"

            roundEntity = RoundEntity()
            roundEntity!!.session = sessionEntity
            roundEntity!!.arrowsPerEnd = 3

            val ydUnit = (data select (com.orengy.archinator.model.orm.Unit::class) where (com.orengy.archinator.model.orm.Unit::name eq "yd")).get().first()

            roundEntity!!.distance = (data select (Distance::class) where
                    (Distance::distance eq 20) and
                    (Distance::unit eq ydUnit)).get().first()
            roundEntity!!.endsCount = 20
            roundEntity!!.targetFace = (data select (TargetFace::class) where
                    (TargetFace::name eq "WA Full")).get().first()
            roundEntity!!.targetFaceSize = (data select (TargetFaceSize::class) where
                    (TargetFaceSize::name eq "60cm") and
                    (TargetFaceSize::targetFace eq roundEntity!!.targetFace)
                    ).get().first()
            roundEntity!!.targetFaceScores = (data select (TargetFaceScores::class) where
                    (TargetFaceSize::name eq "Recurve (10-1)") and
                    (TargetFaceSize::targetFace eq roundEntity!!.targetFace)
                    ).get().first()

//            roundEntity!!.targetFaceScores = (data select (TargetFaceScores::class) where
//                    (TargetFaceSize::name eq "60cm") and
//                    (TargetFaceSize::targetFace eq roundEntity!!.targetFace)
//                    ).get().first()
        }

        adapter = ArrowAdapter()
        adapter.setExecutor(Executors.newSingleThreadExecutor())


        loadDataToViews()


    }

    fun loadDataToViews() {
        sessionName.setText(sessionEntity.title)
        sessionDate.text = Utils.formatTime(sessionEntity.createdAt)

        if (roundEntity != null && roundEntity!!.getFileUrl() != null) {
            Glide.with(this).load(roundEntity!!.getFileUrl()).into(sessionReferenceTargetImageView)
        }


        sessionArrowSpinner.adapter = adapter
        adapter.queryAsync()
    }


    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val photoUrl = PhotoUtils.handleResponse(this, requestCode, resultCode, data)

        if (photoUrl != null) {
            roundEntity!!.localFileUrl = photoUrl
            loadDataToViews()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                finish()
                return true
            }
            R.id.action_save -> {
                if (roundEntity == null)
                    return false
                if (roundEntity!!.getFileUrl() == null)
                    return false

                sessionEntity.title = sessionName.text.toString()
                sessionEntity.equipmentArrow = sessionArrowSpinner.selectedItem as EquipmentArrow

                data.upsert(sessionEntity)

                if (roundEntity != null) {
                    data.upsert(roundEntity!!)
                }

                setResult(Activity.RESULT_OK, Intent())
                finish()
                return true
            }
            R.id.action_reference_photo -> {
                if (roundEntity == null)
                    return false
                PhotoUtils.photoPicker(this, PICK_IMAGE)

            }
        }

        return super.onOptionsItemSelected(item)
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.session_form, menu)
        return true
    }

    private inner class ArrowAdapter :
            QueryAdapter<EquipmentArrow>(Models.DEFAULT, EquipmentArrow::class.java) {
        override fun getView(item: EquipmentArrow?, convertView: View?, parent: ViewGroup?): View {
            val inflater = LayoutInflater.from(parent!!.context)
            val view = inflater.inflate(android.R.layout.simple_spinner_dropdown_item, parent, false) as TextView
            view.text = item!!.name
            return view
        }

        override fun performQuery(): Result<EquipmentArrow> {
            return (data select (EquipmentArrow::class)).get()
        }

    }
}
